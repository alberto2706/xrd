import matplotlib.pyplot as plt
import numpy as np

kim = lambda x : 5.471  - 0.264*x
yam = lambda x : 5.4695 - 0.2704*x

x = np.array([0.0, 0.1])
y_kim = kim(x) 
y_yam = yam(x) 

plt.plot(x, y_kim, '--', label=r'Kim et al., 2006')
plt.plot(x, y_yam, '-',  label=r'Yamanaka et al., 2009')
plt.legend()

plt.show()
