import matplotlib.pyplot as plt
import math
import locale
#plt.style.use('ggplot')
# Set to German locale to get comma decimal separater
#locale.setlocale(locale.LC_ALL,'pt_BR.UTF-8')
print locale.getlocale()


plt.rcdefaults()
plt.rcParams['axes.formatter.use_locale'] = True
lp_00 = 5.47124

x_list  = [1.0, 2.5, 4.0, 9.8]
lp_alpha = [5.47010,5.46985,5.46991,5.46944]
lp_beta  = [5.45679,5.44517,5.44160,5.41969]
wt_alpha = [0.823,0.670,0.528,0.286]
wt_beta  = [1.0 - wt for wt in wt_alpha]

lp_00 = 5.47125
x_list  = [1.0, 2.5, 4.0, 9.8]
lp_alpha = [5.47091,5.46985,5.46988,5.46853]
lp_beta  = [5.4575,5.4444,5.44142,5.42107]
#wt_alpha = [0.774,0.679,0.590,0.260]
wt_beta  = [1.0 - wt for wt in wt_alpha]

#plt.plot(x_list, lp_alpha,'s')
#plt.plot(x_list, lp_beta,'v')
#
#
#plt.show()
#
#plt.plot(x_list, wt_alpha, 's', label=r'$\alpha$')
#plt.plot(x_list, wt_beta, 's', label=r'$\beta$')
#plt.legend()
#plt.show()
#

def wt_to_x(wt_Er2O3):
    #wt_Er2O3 = wt_Er2O3/100.0
    A_O = 15.999
    A_U = 238.02
    A_Er = 167.26
    A_UO2 = A_U + 2.0*A_O
    A_Er2O3 = 2.0*A_Er + 3.0*A_O

    M_pellet = 15.60/3.0 #g
    
    C_Er2O3 = wt_Er2O3*0.01
    C_ADS = 0.2*0.01
    C_UO2 = (1.0 - C_Er2O3 - C_ADS)

    M_UO2 = M_pellet*C_UO2
    M_Er2O3 = M_pellet*C_Er2O3

    N_A = 6.02E23

    n_UO2 = (M_UO2/A_UO2)*N_A
    n_Er2O3 = (M_Er2O3/A_Er2O3)*N_A

    return (n_Er2O3/(0.5*n_UO2))

def fedotov(x):
    return (5.4695 - (wt_to_x(x)*0.2704))

def kim(x):
    return (5.471 - (wt_to_x(x)*0.264)) 

def yamanaka(x):
    return (5.4695 - (wt_to_x(x)*0.2704)) 

def sslp(x):
    x = wt_to_x(x)
    rU4 = 1.0011
    rU5 = 0.88
    rO2 = 1.3675
    rEr3 = 1.004

####rU4 = 1.14
####rU5 = 0.98
####rO2 = 1.42
####rEr3 = 1.144

    result = 0.0
    result += rU4*(1.0-2.0*x)
    result += rU5*x
    result += rEr3*x
    result += (1.0*rO2)
    result = result*4.0/math.sqrt(3.0)
    return result

def linear_regression(x_np, y_np):
    result = {}
    x_array = x_np
    y_array = y_np
    A = np.vstack([x_array, np.ones(len(x_array))]).T
    slope, constant = np.linalg.lstsq(A,y_array)[0]
    res = np.linalg.lstsq(A,y_array)[1]
    r2 = 1 - res / (y_array.size * y_array.var())
    print "slope =", slope 
    print "constant=", constant
    print "r2=", r2
    result['x'] = x_array
    result['y'] = y_array
    result['slope'] = slope
    result['constant'] = constant
    result['r2'] = r2
    return result

import numpy as np

x_np = np.array(x_list)

lp_alpha_np = np.array(lp_alpha)
lp_beta_np = np.array(lp_beta)
alpha_list = zip(lp_alpha, wt_alpha)
beta_list = zip(lp_beta, wt_beta)
x0_list = [0.0] + x_list
w_list = [lp_00]
for i in range(len(lp_alpha)):
	w_list.append(alpha_list[i][0]*alpha_list[i][1] + beta_list[i][0]*beta_list[i][1])




wt_alpha_np = np.array(wt_alpha)
wt_beta_np = np.array(wt_beta)

#lp_np = lp_alpha_np*wt_alpha_np + lp_beta_np*wt_beta_np
x0_np = np.array(x0_list)
w_np = np.array(w_list)
xf_np = np.arange(1.0,9.8,0.001)

lp_fedotov = fedotov(xf_np)
lp_kim = kim(xf_np)
lp_yamanaka = yamanaka(xf_np)
#lp_fedotov = fedotov(x0_np)
print lp_fedotov

lr_alpha = linear_regression(x_np, lp_alpha_np)
lr_beta = linear_regression(x_np, lp_beta_np)
lr = linear_regression(x0_np[0:4], w_np[0:4])
lr_f = linear_regression(xf_np, lp_fedotov)

label_alpha = r'$a_\alpha = {:.5g}x + {:.5g}$, $\chi^2$ = {:.5g}'.format(lr_alpha['slope'],lr_alpha['constant'], lr_alpha['r2'][0])  
label_beta = r'$a_\beta = {:.5g}x + {:.5g}$, $\chi^2$ = {:.5g}'.format(lr_beta['slope'],lr_beta['constant'], lr_beta['r2'][0])
label_aw = 'Este trabalho\n$a_w = {:.5g}x + {:.5g}$, $\chi^2$ = {:.5g}'.format(lr['slope'],lr['constant'], lr['r2'][0])
label_yamanaka = 'Yamanaka (2009)\n$a_y = {:.5g}x + {:.5g}$'.format(-wt_to_x(0.2704),5.4695)
label_kim = 'Kim (2006)\n$a_k = {:.5g}x + {:.5g}$'.format(-wt_to_x(0.264),5.471)

plt.plot((-1.0,10.0),(lp_00,lp_00),'-',c="#cccccc",label='UO${}_2$')
#plt.plot(x_np, lp_alpha_np, 'v', label=r'Phase $\alpha$')
plt.plot(x_list, lp_alpha, 'v', label=r'$\alpha$')
plt.plot(lr_alpha['x'], lr_alpha['x']*lr_alpha['slope'] + lr_alpha['constant'], '-.',label=label_alpha)
plt.plot(x_list, lp_beta, '^', label=r'$\beta$')
plt.plot(lr_beta['x'], lr_beta['x']*lr_beta['slope'] + lr_beta['constant'], '--',label=label_beta)
#plt.plot(x_np[:4], lp_beta_np[:4], '^', label=r'Phase $\beta$')
#plt.plot(x0_np, w_np,'s',label=r'$w_\alpha a_\alpha + w_\beta a_\beta$')
#plt.plot(lr['x'], lr['x']*lr['slope'] + lr['constant'], '-',label=label_aw)
#plt.plot(xf_np, lp_fedotov, '-', label=r'Modelo te$\mathrm{\acute{o}}$rico (Fedotov)')
plt.plot(xf_np, lp_kim, '--', label=label_kim)
plt.plot(xf_np, lp_yamanaka, '-.', label=label_yamanaka)
#plt.plot(lr_alpha['x'], lr_alpha['x']*lr_alpha['slope'] + lr_alpha['constant'], '-.',label=r'$\chi_\alpha^2={}$'.format(lr_alpha['r2'][0]))
#plt.plot(lr_beta['x'], lr_beta['x']*lr_beta['slope'] + lr_beta['constant'], '--',label=r'$\chi_\beta^2={}$'.format(lr_beta['r2'][0]))

#####plt.ylabel(r'Lattice parameter, $\mathrm{\AA}$')
#####plt.xlabel(r'Er${}_2$O${}_3$ content, wt%')
#####plt.legend()
#####plt.show()
###

plt.ylabel(r'Par$\mathrm{\hat{a}}$metro de rede, $\mathrm{\AA}$')
plt.xlabel(r'Teor de Er${}_2$O${}_3$, wt%')
plt.legend()
plt.show()
