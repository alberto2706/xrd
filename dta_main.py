import matplotlib.pyplot as plt

def map_list(input_list, new_min = 0.0, new_max = 1.0):
    output_list = []
    old_min = min(input_list)
    old_max = max(input_list)
    for index in range(len(input_list)):
	old_value = input_list[index]
	new_value = 0.0
	try:
	    new_value = new_min + (old_value - old_min)*(new_max - new_min)/(old_max - old_min)
	    output_list.append(new_value)
	except:
	    print "map_list() except"
    return output_list

def diff_list(x_list, y_list, m = 2):
    dy_list = []
    dx_list = []

    if m == 2:
	for i in range(1, len(y_list) - 1):
	    h = x_list[i + 1] - x_list[i]
	    dy = (y_list[i + 1] - y_list[i - 1])/(2.0*h)
	    dy_list.append(dy)
	    dx_list.append(x_list[i])

    elif m == 4:
	for i in range(2, len(y_list) - 2):
	    h = x_list[i+1] - x_list[i]
	    dy = y_list[i-2] - 8.0*y_list[i-1] + 8.0*y_list[i+1] - y_list[i+2]
	    dy = dy/(12.0*h)
	    dy_list.append(dy)
	    dx_list.append(x_list[i])
    else:
	pass

    return dx_list, dy_list

def load_data(file_path):
	i = []
	x = []
	y = []
	z = []
	w = []
	lines = open(file_path, 'r').read().split('\n')
	for line in lines:
		input_data = line.split(' ')
		if line == '' or line[0] == '#' or len(input_data) < 6:
			pass
		else:
			#print(input_data)
			while '' in input_data:
				input_data.remove('')
			i.append(input_data[0])
			x.append(float(input_data[1]))
			y.append(float(input_data[2]))
			z.append(float(input_data[3]))
			w.append(float(input_data[4]))
	for index in range(len(i)):
		#print i[index], x[index], y[index], z[index], w[index]
		pass

	return i, x, y, z, w
		

i, x, y, z, w = load_data('dta_.txt')
dx,dw = diff_list(x,z,m=4)
popped = 0
index = 0
while index < len(dx) - popped:
	if index % 2 == 0:
		dx.pop(index)
		dw.pop(index)
	else:
		pass
	index += 1

plt.plot(x,map_list(y,0.0,1.0),label=r'Temperatura da amostra, ${}^o$C')
plt.plot(x,map_list(z,0.0,1.0), label=r'TG, $mg$')
plt.plot(x,map_list(w,0.0,1.0), label=r'Fluxo de calor, $\mu V$')
#plt.plot(dx,map_list(dw,-10.0,10.0))
plt.xlabel('Tempo, $h$')
plt.legend()
plt.show()
