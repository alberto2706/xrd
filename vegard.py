import matplotlib.pyplot as plt
import math

lp_00 = 5.471
lp_alpha = []
lp_beta = []
wt_alpha = []
wt_beta = []
result_alpha = zip(lp_alpha, wt_alpha)
result_beta  = zip(lp_beta, wt_beta)

def wt_to_x(wt, el = 'Er'):
    result = 0.0
    return result

def lp(y, rR3):
    rU4 = 1.00
    rU5 = 0.84
    rO2 = 1.38
    result = (4.0/math.sqrt(3.0))*((1.0-2.0*y)*rU4 + y*rU5 + y*rR3 + rO2)
    return result

rGd3 = 1.053
rEr3 = 1.004
rY3 = 0.985

y = 0.0
x_list = list()
yGd_list = list()
yEr_list = list()
yY_list = list()
while y < 0.4:
    x_list.append(y)
    yGd_list.append(lp(y,rGd3))
    yEr_list.append(lp(y,rEr3))
    yY_list.append(lp(y,rY3))
    y += 0.001


plt.plot(x_list,yGd_list,label='Gd')
plt.plot(x_list,yEr_list,label='Er')
plt.plot(x_list,yY_list,label='Y')
plt.legend()
plt.show()
