import matplotlib.pyplot as plt
import numpy as np
import math

color00 = '#1f77b4'
color10 = '#ff7f0e'
color25 = '#2ca02c'
color40 = '#d62728'
color98 = '#9467bd'


default_arrow_style = {}
default_arrow_style['ec'] = 'k'
default_arrow_style['fc'] = 'k'
default_arrow_style['head_width'] = 0.05/2.0
default_arrow_style['head_length'] = 0.1/2.0
#fig = plt.figure()
#ax = fig.gca()
#ax.set_aspect('equal')
#plt.yticks([])
#plt.xticks([])
#ax.axis('off')
#fig.patch.set_visible(False)

class DataXY(object):
    def __init__(self, file_path = '', label = ''):
	self.x = []
	self.y = []
	self.file_path = file_path
	self.slope = 0.0
	self.intercept = 0.0
	self.r2 = 0.0
	self.plot = None
	self.color = None
	self.label = label
	if file_path:
	    self.load_data_xy(file_path)
	    
    def load_data_xy(self, data_file_path):
	file_string_lines = open(data_file_path, 'r').read().split('\n')
	print 'loading: ', data_file_path
	for line in file_string_lines:
	    input_data = line.split()
	    if len(input_data) > 1:
		try:
		    self.x.append(float(input_data[0]))
		    self.y.append(float(input_data[1]))
		except:
		    print 'load_data_xy: except',
		    print line
	    else:
	     pass
	print 'file loaded'


    def plot_data(self, map_to=None, x_range = (), style='-+', label = None, color=None):
	x_list = []
	y_list = []
	if x_range:
	    x_min_index = 0
	    x_max_index = len(self.x) - 1
	    while x_min_index < len(self.x):
		if self.x[x_min_index] >= x_range[0]:
		    break
		x_min_index += 1
	    while x_max_index > 0:
		if self.x[x_max_index] <= x_range[1]:
		    break
		x_max_index -= 1
#x_min_index = self.x.index(x_range[0])
#x_max_index = self.x.index(x_range[1])
	else:
	    x_min_index = 0
	    x_max_index = len(self.x) - 1
	if map_to is None:
	    y_list = self.y[x_min_index:x_max_index]
	else:
	    y_list = map_list(self.y[x_min_index:x_max_index], map_to[0], map_to[1])
#y_list = self.map_y(map_to[0], map_to[1])
	x_list = self.x[x_min_index:x_max_index]
#y_list = y_list[x_min_index:x_max_index]

	for y_index in range(len(y_list)):
	    pass
#y_list[y_index] = math.sqrt(y_list[y_index])
	if label is None:
	    self.plot = plt.plot(x_list, y_list, style)
	else:
	    self.plot = plt.plot(x_list, y_list, style, c=color, label=label)
#plt.legend()
	    
	self.color = self.plot[0]._color
	self.label = ''

    def map_y(self, new_y_min, new_y_max):
	result = []
	old_y_min = min(self.y)
	old_y_max = max(self.y)
	for index in xrange(len(self.x)):
	    old_y = self.y[index]
	    new_y = 0.0
	    try:
		new_y = new_y_min + (old_y - old_y_min)*(new_y_max - new_y_min)/(old_y_max - old_y_min)
		result.append(new_y)
	    except:
		print 'map_y: except'
		print new_y
	return result
    
    def linear_regression(self):
	x_array = np.array(self.x)
	y_array = np.array(self.y)
	A = np.vstack([x_array, np.ones(len(x_array))]).T
	slope, intercept = np.linalg.lstsq(A,y_array)[0]
	res = np.linalg.lstsq(A,y_array)[1]
	r2 = 1 - res / (y_array.size * y_array.var())
	print "slope =", slope 
	print "intercept=", intercept
	print "r2=", r2
	self.slope = slope
	self.intercept = intercept
	self.r2 = r2

    def plot_regression(self, style='--', color=False, line_label=False):
	regression = self.linear_regression()
	x_array = np.array(self.x[:len(self.x)-1])
#x_array = np.array(self.x)
	if not color:
	    color = self.color
	if line_label == False:
	    line_label = self.label
	if line_label:
	    plt.plot(x_array, self.slope*x_array + self.intercept, linestyle=style, c=color, label=line_label)
	    plt.legend()
	else:
	    plt.plot(x_array, self.slope*x_array + self.intercept, linestyle=style, c=color)
	    print line_label
	    print self.label


def map_value(value, new_y_min, new_y_max, old_y_min, old_y_max):
    old_y = value
    new_y = 0.0
    try:
	new_y = new_y_min + (old_y - old_y_min)*(new_y_max - new_y_min)/(old_y_max - old_y_min)
    except:
	print 'map_value: except'
    return new_y

def map_list(input_list, new_y_min, new_y_max):
    result = []
    old_y_min = min(input_list[1:])
    old_y_max = max(input_list[1:])
    for index in xrange(len(input_list)):
	old_y = input_list[index]
	new_y = 0.0
	try:
	    new_y = new_y_min + (old_y - old_y_min)*(new_y_max - new_y_min)/(old_y_max - old_y_min)
	    result.append(new_y)
	except:
	    print 'map_list: except'
    return list(result)

def plot_line(x_list, slope, intercept, style='-+', line_label=False, color='r' ):
    x_array = np.array(x_list)
    if line_label:
	plt.plot(x_array, slope*x_array + intercept, linestyle=style, c=color, label=line_label)
	plt.legend()
    else:
	plt.plot(x_array, slope*x_array + intercept, linestyle=style, c=color)

def load_hkl_from_csv(data_file_path):
    phases = []
    curr_phase = []
    file_lines = open(data_file_path, 'r').read().split('\n')
    for line_index, line in enumerate(file_lines):
	entries = line.split(',')
	if '\"' in line: #i.e. new phase entry
	    if len(curr_phase):
		phases.append(list(curr_phase))
	    curr_phase = []
	    for entry in entries:
		curr_phase.append([entry.strip('\"')])
	elif len(entries) > 1:
	    print entries
	    for entry_index, entry in enumerate(entries):
		curr_phase[entry_index].append(entry)
	else:
	    pass
    phases.append(list(curr_phase))
    return phases


def load_data_hkl(data_file_path):
    result = []
    file_string_lines = open(data_file_path, 'r').read().split('\n')
    for line_index in range(len(file_string_lines)):
	line = file_string_lines[line_index]
	line_data = line.split()
	if len(line.split()) > 1 and line.split()[0] == 'PWDR':
	    pass
	elif len(line.split()) > 1 and line.split()[0] == 'h':
	    table = {}
	    for entry_index in range(len(line_data)):
		entry = line_data[entry_index]
		result.append([entry])
		table[entry] = entry_index 
	else:
	    for entry_index in range(len(line_data)):
		try:
		    entry = line_data[entry_index]
		    result[entry_index].append(float(entry))
		except:
		    print entry
		    for r in result:
			print r
		    print entry_index
		    for k in table:
			print k
    return (result, table)

def print_hkl_phases(phase, bar_color = color00, marker_style='v',map_to=(),phase_label='',phase_marker='v'):
    hkl_count = len(phase[0])
    x_list = phase[3]
    y_list = phase[5]
    labeled = False
    if map_to:
	y_list = [y_list[0]] + map_list(list([float(y) for y in phase[5][1:]]), map_to[0], map_to[1])
    for i in range(1,hkl_count):
	tth = x_list[i]
	Fc2 = y_list[i]
	plt.plot((tth,tth),(0,Fc2), c=bar_color)
	if not labeled:
	    labeled = True
	    plt.plot(tth, Fc2, phase_marker, color=bar_color, label=phase_label)
	else:
	    plt.plot(tth, Fc2, phase_marker, color=bar_color)
#plt.legend()




def plot_hkl_marker(hkl_data, table, marker_style = 'v', line_color = 'red', hkl_text = False, 
	     map_to=None, label=None):
    lines = len(hkl_data[0])
    tth_index = table['2-theta']
    Fc_index = table['Fc**2']
    tth_list = hkl_data[tth_index]
    Fc_list = hkl_data[Fc_index]
    labeled = False

    if map_to is None:
	y_list = [(0.0, f) for f in Fc_list]
    else:
	y_list = [(map_to[0], map_value(f, map_to[0], map_to[1], 0.0, max(Fc_list[1:]))) for f in Fc_list]

    for index in range(1,lines):
	tth = tth_list[index]
	Fc = y_list[index]
	plt.plot((tth, tth),(Fc[0],Fc[1]), '-', c=line_color)
	if labeled == False:
	    labeled = True
	    plt.plot(tth,Fc[1], marker_style, label=label, c=line_color)
	else:
	    plt.plot(tth,Fc[1], marker_style, c=line_color)

def plot_hkl(hkl_data, table, line_style = '--', line_color = 'red', hkl_text = False, 
	     map_to=None):
    lines = len(hkl_data[0])
    tth_index = table['2-theta']
#Fc_index = table['I100']
    Fc_index = table['Fc**2']
    tth_list = hkl_data[tth_index]
    Fc_list = hkl_data[Fc_index]

    if map_to is None:
	y_list = [(0.0, f) for f in Fc_list]
    else:
	y_list = [(map_to[0], map_value(f, map_to[0], map_to[1], 0.0, max(Fc_list[1:]))) for f in Fc_list]

    for index in range(1,lines):
	tth = tth_list[index]
	Fc = y_list[index]
	plt.plot((tth,tth),(Fc[0],Fc[1]), line_style)

class Vector(object):
    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
	self.x = x
	self.y = y
	self.z = z

    def length(self):
	result = 0.0
	result += self.x*self.x
	result += self.y*self.y
	result += self.z*self.z
	return math.sqrt(result)

    def normalized(self):
	result = Vector()
	result = self*(1.0/self.length())
	return result

    def __add__(self, other):
	result = Vector()
	result.x = self.x + other.x
	result.y = self.y + other.y
	result.z = self.z + other.z
	return result

    def __sub__(self, other):
	result = Vector()
	result.x = self.x - other.x
	result.y = self.y - other.y
	result.z = self.z - other.z
	return result

    def __mul__(self, other): #prod escalar
	if type(other) == float or type(other) == int:
	    result = Vector()
	    result.x = self.x*other
	    result.y = self.y*other
	    result.z = self.z*other
	elif type(other) == Vector:
	    result = 0.0
	    result += self.x*other.x
	    result += self.y*other.y
	    result += self.z*other.z
	else:
	    exit(1)
	return result

    def __xor__(self, other): #prod vetorial
	result = Vector()
	result.x = self.y*other.z - self.z*other.y
	result.y = self.z*other.x - self.x*other.z
	result.z = self.x*other.y - self.y*other.x
	return result

    def __str__(self):
	result = '[%f, %f, %f]' % (self.x, self.y, self.z)
	return result

    def rotate(self,angle):
	th = angle*math.pi/180.0
	result = Vector()
	cos_th = math.cos(th)
	sin_th = math.sin(th)
	result.x = self.x*cos_th + self.y*sin_th
	result.y = -self.x*sin_th + self.y*cos_th
	return result

    def translate(self, shift):
	result = Vector()
	result = self + shift
	return result

def draw_line(V1, V2, shift = None, color = None):
    lc = 'k'
    if color:
	lc = color
    if shift is None:
	shift = Vector()

    if type(V1) == type(tuple()):
	V1 = Vector(V1[0], V1[1])
    if type(V2) == type(tuple()):
	V2 = Vector(V2[0], V2[1])
    if type(shift) == type(tuple()):
	shift = Vector(shift[0], shift[1])
	
    P1 = (V1.x + shift.x, V2.x + shift.x)
    P2 = (V1.y + shift.y, V2.y + shift.y)
    p = plt.plot(P1, P2, color=lc)

def draw_list(line_list, cycle = False, shift=None):
    P1 = line_list[0]
    for index in range(1,len(line_list)):
	P2 = line_list[index]
	draw_line(P1,P2,shift)
	P1 = line_list[index]
    if cycle:
	draw_line(P1, line_list[0],shift)

def draw_rect(pos, dim, shift = (0.0, 0.0)):
    if type(pos) == type(tuple()):
	pos = Vector(pos[0], pos[1])
    if type(dim) == type(tuple()):
	dim = Vector(dim[0], dim[1])
    if type(shift) == type(tuple()):
	shift = Vector(shift[0], shift[1])
    pos = pos + shift
    x_dim = Vector(dim.x, 0.0)
    y_dim = Vector(0.0, dim.y)
    draw_line(pos, pos + x_dim)
    draw_line(pos + x_dim, pos + dim)
    draw_line(pos + dim, pos + y_dim)
    draw_line(pos + y_dim, pos)

def draw_arrow(from_pos, to_pos, arrow_style = default_arrow_style):
    for arg in default_arrow_style:
	    if not arg in arrow_style:
		    arrow_style[arg] = default_arrow_style[arg]
    
    x1 = float(from_pos.x)
    y1 = float(from_pos.y)
    x2 = float(to_pos.x)
    y2 = float(to_pos.y)
    dx = x2 - x1
    dy = y2 - y1

    ax.arrow(x1,y1,dx,dy,**arrow_style)
    
def draw_wave(from_pos, to_pos, A = 1.0, n = 1):
    L = (to_pos - from_pos).length()
    k = (to_pos - from_pos)*(1.0/(to_pos-from_pos).length())
    draw_vec = from_pos
    print L
    print k
    print draw_vec
    x = 0.0
    x_vec = from_pos
    sin_f = math.sin(2.0*math.pi*float(n)*(x)/L)
    amplitude = Vector(k.x, k.y).rotate(90.0)
    amplitude = amplitude*A
    amplitude = amplitude*sin_f
    amplitude = amplitude + x_vec
    draw_vec = Vector(amplitude.x, amplitude.y)
    dx = L*0.01
    while x < L:
	x += dx
	x_vec = k*x
	x_vec = x_vec + from_pos
	sin_f = math.sin(2.0*math.pi*float(n)*(x)/L)
	amplitude = Vector(k.x, k.y).rotate(90.0)
	amplitude = amplitude*A
	amplitude = amplitude*sin_f
	amplitude = amplitude + x_vec
	new_draw_vec = Vector(amplitude.x, amplitude.y)
	draw_line(draw_vec, new_draw_vec)
	draw_vec = Vector(new_draw_vec.x, new_draw_vec.y)

data00 = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_00.xy', label = 'UO${}_2$-0,0wt%Er${}_2$O${}_3$')
data10 = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_10.xy', label = 'UO${}_2$-1,0wt%Er${}_2$O${}_3$')
data25 = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_25.xy', label = 'UO${}_2$-2,5wt%Er${}_2$O${}_3$')
data40 = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_40.xy', label = 'UO${}_2$-4,0wt%Er${}_2$O${}_3$')
data98 = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_98.xy', label = 'UO${}_2$-9,8wt%Er${}_2$O${}_3$')
#data00.plot_data(map_to=(0.0,1.5), x_range = (), style='-', label = data00.label, color=color00)
#data10.plot_data(map_to=(1.0,2.5), x_range = (), style='-', label = data10.label, color=color10)
#data25.plot_data(map_to=(2.0,3.5), x_range = (), style='-', label = data25.label, color=color25)
#data40.plot_data(map_to=(3.0,4.5), x_range = (), style='-', label = data40.label, color=color40)
#data98.plot_data(map_to=(4.0,5.5), x_range = (), style='-', label = data98.label, color=color98)
#plt.text(60,0.4, '0,0')
#plt.text(60,1.4, '1,0')
#plt.text(60,2.4, '2,5')
#plt.text(60,3.4, '4,0')
#plt.text(60,4.4, '9,8')
#
#plt.show()
#exit()

data_uo2_pwd = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2-INB.xy',     label='UO2')
data_er2o3_pwd = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/Er2O3.xy',     label='Er2O3 PWD')
data_er2o3_dta = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/Er2O3_DTA.xy', label='Er2O3 DTA')
data_er2o3_nan = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/Er2O3nano.xy', label='Er2O3 NANO')
data_zeo = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/zeolita.xy', label='Er2O3 NANO')

data_zeo.plot_data(map_to=(1.02,1.12), x_range = (22,65), style='-', label = data98.label, color=color98)
data_uo2_pwd.plot_data(map_to=(1.12,1.22), x_range = (22,65), style='-', label = data98.label, color=color10)

data_zeo.plot_data(map_to=(0.0,0.2), x_range = (22,65), style='-', label = data98.label, color=color25)
data_uo2_pwd.plot_data(map_to=(0.0,1.0), x_range = (22,65), style='-', label = data98.label, color=color25)
plt.yticks([])
plt.show()
exit()

data_list = []
data_list.append(data00)
data_list.append(data10)
data_list.append(data25)
data_list.append(data40)
data_list.append(data98)

def fig_raw2():
    fig = plt.figure()
    ax = fig.gca()
    data_er2o3_pwd.plot_data(map_to=(0.01+1.0,2.0), x_range = (), style='-', label = 'Er2O3', color=color10)
    data_uo2_pwd.plot_data(map_to=(0.0,1.0-0.01), x_range = (), style='-', label = 'UO2', color=color00)

    plt.yticks([])
    plt.xlabel(r'$2\theta$, graus')
    plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')

    ax.set_xlim(10,90)
    plt.legend()
    plt.show()


def fig_superpos():
    plt.yticks([])
    plt.xticks([])
    x_array = np.arange(20.8, 28.4+2,0.01)
    y1_array = 0.8*np.exp(-(x_array - 25.0)**2/1.6)
    y2_array = 0.4*np.exp(-(x_array - 25.8)**2/1.6)
    y_array = y1_array + y2_array
    plt.plot(x_array, y1_array, label='Plano $h_1k_1l_1$')
    plt.plot((25.0, 25.0), (0.0, 0.8), '--')
    plt.text(25.0, -0.02, '$h_1k_1l_1$')
    plt.plot((25.8, 25.8), (0.0, 0.4), '--')
    plt.text(25.8, -0.02, '$h_2k_2l_2$')
    plt.plot(x_array, y2_array, label='Plano $h_2k_2l_2$')
#plt.plot(x_array, y_array, label='Pefil observado')
    plt.plot(x_array, y_array, 'o', markevery=12, label='Pefil observado')
    plt.legend()
    plt.show()
    exit()

def fig_sim_xrd():
    fig = plt.figure()
    ax = fig.gca()
    ax.set_xlim(45,60)
    plt.yticks([])
    import random
    plt.xlabel(r'$2\theta$, graus')
    plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')
    plt.plot(data25.x, data25.y, '.', markevery=2, label='$Y_{o,i}$, Intensidade observada')
    plt.plot(data25.x, [v*float(random.randint(7, 10))/10.0 for v in data25.y], '-', markevery=8, label='$Y_{c,i}$, Intensidade calculada')
    plt.legend()
    plt.show()
    exit()




def fig_rietveld():
    x_array = np.arange(16.0, 46.0, 0.2)
    peaks = [(1.0, 19.3), (0.5, 26.0), (0.58, 33.0)]
    y_c = lambda x : 0.02 + sum([p[0]*np.exp(-(p[1]-x)**2) for p in peaks])
    y_array = y_c(x_array)

    for p in peaks:
	plt.plot((p[1],p[1]), (0.0,p[0]),'--')


    y_noise = [x + .03*(-1)**(np.random.randint(2))*np.random.rand()/(18.8*x)/1.0 for x in list(y_array)]
    

    plt.plot(x_array, y_array)
    plt.plot(x_array, y_noise,'d')
    plt.show()
    exit()


def fig_raw():
    fig = plt.figure()

    plt.subplot(211)
    ax = fig.gca()
    plt.yticks([])
    plt.title('UO2-INB')
    plt.xlabel(r'$2\theta$, graus')
    ax.set_xlim(20,90)
    plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')
    plt.plot(data_uo2_pwd.x, data_uo2_pwd.y, '-',c=color00)

    plt.subplot(212)
    ax = fig.gca()
    plt.yticks([])
    plt.title('Er2O3')
    plt.xlabel(r'$2\theta$, graus')
    ax.set_xlim(20,90)
    plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')
    plt.plot(data_er2o3_pwd.x, data_er2o3_pwd.y, '-',c=color10)

    plt.show()

def fig_xrd():
    fig = plt.figure()
    ax = fig.gca()
    data98.plot_data(map_to=(0,1),x_range=(20,90), style='-', color=color98,label='')

    plt.yticks([])
    plt.xlabel(r'$2\theta$, graus')
    plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')
    ax.set_xlim(20,90)
    plt.show()


def fig_00_10():
    pad = 0.4
    fig = plt.figure(1)
    plt.subplot(131)
    plt.yticks([])
    data00.plot_data(map_to=(0.0/3.0-pad,1.0/3.0+pad),x_range=(27.5,29.0),style='-+',label=r'UO${}_2$')
    data10.plot_data(map_to=(0.0/3.0-pad,1.0/3.0+pad),x_range=(27.5,29.0),style='--',label=r'UO${}_2$-1,0wt% Er${}_2$O${}_3$')
    plt.subplot(132)
    plt.yticks([])
    data00.plot_data(map_to=(0.0/3.0-pad,1.0/3.0+pad),x_range=(45,49),style='-+',label=r'UO${}_2$')
    data10.plot_data(map_to=(0.0/3.0-pad,1.0/3.0+pad),x_range=(45,49),style='--',label=r'UO${}_2$-1,0wt% Er${}_2$O${}_3$')
    plt.subplot(133)
    plt.yticks([])
    data00.plot_data(map_to=(0.0/3.0-pad,1.0/3.0+pad),x_range=(74,79.5),style='-+',label=r'UO${}_2$')
    data10.plot_data(map_to=(0.0/3.0-pad,1.0/3.0+pad),x_range=(74,79.5),style='--',label=r'UO${}_2$-1,0wt% Er${}_2$O${}_3$')
    plt.legend()

    plt.show()
    exit()

def fig_98_er2o3():
    pad = 0.4
    fig = plt.figure(1)
    ax = fig.gca()
    ax.set_xlim(20,90)
#plt.subplot(131)
    plt.yticks([])
    data98.plot_data(map_to=(4.0/5.0-pad,5.0/5.0+pad),x_range=(),style='-',label=r'UO${}_2$-9,8wt% Er${}_2$O${}_3$',color=color98)
    data_er2o3_pwd.plot_data(map_to=(4.0/5.0-pad,5.0/5.0+pad),x_range=(),style='-',label=r'Er${}_2$O${}_3$',color='#42D34D')
    plt.legend()
    plt.show()
#fig_98_er2o3()

def fig_00_10_25():
    pad = 0.4
    fig = plt.figure(1)
    plt.subplot(131)
    plt.yticks([])
    data00.plot_data(map_to=(0.0/5.0-pad,1.0/5.0+pad),x_range=(27.5,29.0+1.0),style='-',label=r'UO${}_2$')
    data10.plot_data(map_to=(1.0/5.0-pad,2.0/5.0+pad),x_range=(27.5,29.0+1.0),style='-',label=r'UO${}_2$-1,0wt% Er${}_2$O${}_3$')
    data25.plot_data(map_to=(2.0/5.0-pad,3.0/5.0+pad),x_range=(27.5,29.0+1.0),style='-',label=r'UO${}_2$-2,5wt% Er${}_2$O${}_3$')
    data40.plot_data(map_to=(3.0/5.0-pad,4.0/5.0+pad),x_range=(27.5,29.0+1.0),style='-',label=r'UO${}_2$-4,0wt% Er${}_2$O${}_3$')
    data98.plot_data(map_to=(4.0/5.0-pad,5.0/5.0+pad),x_range=(27.5,29.0+1.0),style='-',label=r'UO${}_2$-9,8wt% Er${}_2$O${}_3$')
    plt.subplot(132)
    plt.yticks([])
    data00.plot_data(map_to=(0.0/5.0-pad,1.0/5.0+pad),x_range=(45,49),style='-',label=r'UO${}_2$')
    data10.plot_data(map_to=(1.0/5.0-pad,2.0/5.0+pad),x_range=(45,49),style='-',label=r'UO${}_2$-1,0wt% Er${}_2$O${}_3$')
    data25.plot_data(map_to=(2.0/5.0-pad,3.0/5.0+pad),x_range=(45,49),style='-',label=r'UO${}_2$-2,5wt% Er${}_2$O${}_3$')
    data40.plot_data(map_to=(3.0/5.0-pad,4.0/5.0+pad),x_range=(45,49),style='-',label=r'UO${}_2$-4,0wt% Er${}_2$O${}_3$')
    data98.plot_data(map_to=(4.0/5.0-pad,5.0/5.0+pad),x_range=(45,49),style='-',label=r'UO${}_2$-9,8wt% Er${}_2$O${}_3$')
    plt.subplot(133)
    plt.yticks([])
    data00.plot_data(map_to=(0.0/5.0-pad,1.0/5.0+pad),x_range=(74,79.5+2.0),style='-',label=r'UO${}_2$')
    data10.plot_data(map_to=(1.0/5.0-pad,2.0/5.0+pad),x_range=(74,79.5+2.0),style='-',label=r'UO${}_2$-1,0wt% Er${}_2$O${}_3$')
    data25.plot_data(map_to=(2.0/5.0-pad,3.0/5.0+pad),x_range=(74,79.5+2.0),style='-',label=r'UO${}_2$-2,5wt% Er${}_2$O${}_3$')
    data40.plot_data(map_to=(3.0/5.0-pad,4.0/5.0+pad),x_range=(74,79.5+2.0),style='-',label=r'UO${}_2$-4,0wt% Er${}_2$O${}_3$')
    data98.plot_data(map_to=(4.0/5.0-pad,5.0/5.0+pad),x_range=(74,79.5+2.0),style='-',label=r'UO${}_2$-9,8wt% Er${}_2$O${}_3$')
    plt.legend()

    plt.show()
    exit()



#fig_raw()
#phases =  load_hkl_from_csv('../apres/final00.csv')
#print phases[0]
#print_hkl_phases(phases[0], bar_color = color00, marker_style='v',map_to=(0,1),phase_label='',phase_marker='v')
#fig_raw2()

#fig_xrd()
#fig_superpos()
#fig_00_10_25()

fig_sim_xrd()
