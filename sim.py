import math
import cromermann as cm

modulus = lambda a : a[0]**2 + a[1]**2

class Vector(object):
    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
	self.x = x
	self.y = y
	self.z = z

    def length(self):
	result = 0.0
	result += self.x*self.x
	result += self.y*self.y
	result += self.z*self.z
	return math.sqrt(result)

    def __add__(self, other):
	result = Vector()
	result.x = self.x + other.x
	result.y = self.y + other.y
	result.z = self.z + other.z
	return result

    def __sub__(self, other):
	result = Vector()
	result.x = self.x - other.x
	result.y = self.y - other.y
	result.z = self.z - other.z
	return result

    def __mul__(self, other):
	if type(other) == float or type(other) == int:
	    result = Vector()
	    result.x = self.x*other
	    result.y = self.y*other
	    result.z = self.z*other
	elif type(other) == Vector:
	    result = 0.0
	    result += self.x*other.x
	    result += self.y*other.y
	    result += self.z*other.z
	else:
	    exit(1)
	return result

    def __xor__(self, other): #prod vetorial
	result = Vector()
	result.x = self.y*other.z - self.z*other.y
	result.y = self.z*other.x - self.x*other.z
	result.z = self.x*other.y - self.y*other.x
	return result

    def __str__(self):
	result = '[%f, %f, %f]' % (self.x, self.y, self.z)
	return result

class Atom(object):
    def __init__(self, ion = 'H', pos = Vector(), label=''):
	self.pos = pos
	self.ion = ion
	if not label:
	    self.label = ion
	else:
	    self.label = label

    def form_factor(self, Q):
	Q_over_4pi = Q/(4.0*math.pi)
	cm_coeff = cm.coeff[self.ion]
	a = cm_coeff[:4]
	b = cm_coeff[5:]
	c = cm_coeff[4]
	result = 0.0
	result = c + sum(a_i*math.exp(-b_i*math.pow(Q_over_4pi, 2.0)) for a_i, b_i in zip(a,b))
	return result

class Crystal(object):
    def __init__(self, atoms = None):
	self.atoms = atoms

    def structure_factor(self, Q):
	F_Re, F_Im = 0.0, 0.0
	Q_len = Q.length()
	F_Re = sum(a.form_factor(Q_len)*math.cos(a.pos*Q) for a in self.atoms)
	F_Im = sum(a.form_factor(Q_len)*math.sin(a.pos*Q) for a in self.atoms)
	return (F_Re, F_Im)

uo2 = []
uo2.append(Atom('U4+', Vector(0.0, 0.0, 0.0)))
uo2.append(Atom('U4+', Vector(0.5, 0.0, 0.0)))
uo2.append(Atom('U4+', Vector(0.0, 0.5, 0.0)))
uo2.append(Atom('U4+', Vector(0.0, 0.0, 0.5)))

uo2.append(Atom('O2-', Vector(0.25, 0.25, 0.25)))
uo2.append(Atom('O2-', Vector(0.75, 0.25, 0.25)))
uo2.append(Atom('O2-', Vector(0.25, 0.75, 0.25)))
uo2.append(Atom('O2-', Vector(0.75, 0.75, 0.25)))

uo2.append(Atom('O2-', Vector(0.25, 0.25, 0.75)))
uo2.append(Atom('O2-', Vector(0.75, 0.25, 0.75)))
uo2.append(Atom('O2-', Vector(0.25, 0.75, 0.75)))
uo2.append(Atom('O2-', Vector(0.75, 0.75, 0.75)))

print(modulus(Crystal(uo2).structure_factor(Vector(1,0,0))))
print(modulus(Crystal(uo2).structure_factor(Vector(2,0,0))))
print(modulus(Crystal(uo2).structure_factor(Vector(1,1,1))))


