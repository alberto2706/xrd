import matplotlib.pyplot as plt
import numpy as np
import math

#plt.style.use('ggplot')

color00 = '#1f77b4'
color10 = '#ff7f0e'
color25 = '#2ca02c'
color40 = '#d62728'
color98 = '#9467bd'

class DataXY(object):
    def __init__(self, file_path = '', label = ''):
	self.x = []
	self.y = []
	self.file_path = file_path
	self.slope = 0.0
	self.intercept = 0.0
	self.r2 = 0.0
	self.plot = None
	self.color = None
	self.label = label
	if file_path:
	    self.load_data_xy(file_path)
	    
    def load_data_xy(self, data_file_path):
	file_string_lines = open(data_file_path, 'r').read().split('\n')
	print 'loading: ', data_file_path
	for line in file_string_lines:
	    input_data = line.split()
	    if len(input_data) > 1:
		try:
		    self.x.append(float(input_data[0]))
		    self.y.append(float(input_data[1]))
		except:
		    print 'load_data_xy: except',
		    print line
	    else:
	     pass
	print 'file loaded'


    def plot_data(self, map_to=None, x_range = (), style='-+', label = None, color=None):
	x_list = []
	y_list = []
	if x_range:
	    x_min_index = 0
	    x_max_index = len(self.x) - 1
	    while x_min_index < len(self.x):
		if self.x[x_min_index] >= x_range[0]:
		    break
		x_min_index += 1
	    while x_max_index > 0:
		if self.x[x_max_index] <= x_range[1]:
		    break
		x_max_index -= 1
#x_min_index = self.x.index(x_range[0])
#x_max_index = self.x.index(x_range[1])
	else:
	    x_min_index = 0
	    x_max_index = len(self.x) - 1
	if map_to is None:
	    y_list = self.y[x_min_index:x_max_index]
	else:
	    y_list = map_list(self.y[x_min_index:x_max_index], map_to[0], map_to[1])
#y_list = self.map_y(map_to[0], map_to[1])
	x_list = self.x[x_min_index:x_max_index]
#y_list = y_list[x_min_index:x_max_index]

	for y_index in range(len(y_list)):
	    pass
#y_list[y_index] = math.sqrt(y_list[y_index])
	if label is None:
	    self.plot = plt.plot(x_list, y_list, style)
	else:
	    self.plot = plt.plot(x_list, y_list, style, c=color, label=label)
#plt.legend()
	    
	self.color = self.plot[0]._color
	self.label = ''

    def map_y(self, new_y_min, new_y_max):
	result = []
	old_y_min = min(self.y)
	old_y_max = max(self.y)
	for index in xrange(len(self.x)):
	    old_y = self.y[index]
	    new_y = 0.0
	    try:
		new_y = new_y_min + (old_y - old_y_min)*(new_y_max - new_y_min)/(old_y_max - old_y_min)
		result.append(new_y)
	    except:
		print 'map_y: except'
		print new_y
	return result
    
    def linear_regression(self):
	x_array = np.array(self.x)
	y_array = np.array(self.y)
	A = np.vstack([x_array, np.ones(len(x_array))]).T
	slope, intercept = np.linalg.lstsq(A,y_array)[0]
	res = np.linalg.lstsq(A,y_array)[1]
	r2 = 1 - res / (y_array.size * y_array.var())
	print "slope =", slope 
	print "intercept=", intercept
	print "r2=", r2
	self.slope = slope
	self.intercept = intercept
	self.r2 = r2

    def plot_regression(self, style='--', color=False, line_label=False):
	regression = self.linear_regression()
	x_array = np.array(self.x[:len(self.x)-1])
#x_array = np.array(self.x)
	if not color:
	    color = self.color
	if line_label == False:
	    line_label = self.label
	if line_label:
	    plt.plot(x_array, self.slope*x_array + self.intercept, linestyle=style, c=color, label=line_label)
	    plt.legend()
	else:
	    plt.plot(x_array, self.slope*x_array + self.intercept, linestyle=style, c=color)
	    print line_label
	    print self.label

class DataHKL(object):
    pass

def map_value(value, new_y_min, new_y_max, old_y_min, old_y_max):
    old_y = value
    new_y = 0.0
    try:
	new_y = new_y_min + (old_y - old_y_min)*(new_y_max - new_y_min)/(old_y_max - old_y_min)
    except:
	print 'map_value: except'
    return new_y

def map_list(input_list, new_y_min, new_y_max):
    result = []
    old_y_min = min(input_list[1:])
    old_y_max = max(input_list[1:])
    for index in xrange(len(input_list)):
	old_y = input_list[index]
	new_y = 0.0
	try:
	    new_y = new_y_min + (old_y - old_y_min)*(new_y_max - new_y_min)/(old_y_max - old_y_min)
	    result.append(new_y)
	except:
	    print 'map_list: except'
    return result

def plot_line(x_list, slope, intercept, style='-+', line_label=False, color='r' ):
    x_array = np.array(x_list)
    if line_label:
	plt.plot(x_array, slope*x_array + intercept, linestyle=style, c=color, label=line_label)
	plt.legend()
    else:
	plt.plot(x_array, slope*x_array + intercept, linestyle=style, c=color)

def load_data_hkl(data_file_path):
    result = []
    file_string_lines = open(data_file_path, 'r').read().split('\n')
    for line_index in range(len(file_string_lines)):
	line = file_string_lines[line_index]
	line_data = line.split()
	if len(line.split()) > 1 and line.split()[0] == 'PWDR':
	    pass
	elif len(line.split()) > 1 and line.split()[0] == 'h':
	    table = {}
	    for entry_index in range(len(line_data)):
		entry = line_data[entry_index]
		result.append([entry])
		table[entry] = entry_index
	else:
	    for entry_index in range(len(line_data)):
		entry = line_data[entry_index]
		result[entry_index].append(float(entry))
    return (result, table)

def plot_hkl_marker(hkl_data, table, marker_style = 'v', line_color = 'red', hkl_text = False, 
	     map_to=None, label=None):
    lines = len(hkl_data[0])
    tth_index = table['2-theta']
    Fc_index = table['Fc**2']
    tth_list = hkl_data[tth_index]
    Fc_list = hkl_data[Fc_index]
    h_index = table['h']
    k_index = table['k']
    l_index = table['l']
    h_list = hkl_data[h_index]
    k_list = hkl_data[k_index]
    l_list = hkl_data[l_index]
    labeled = False

    if map_to is None:
	y_list = [(0.0, f) for f in Fc_list]
    else:
	y_list = [(map_to[0], map_value(f, map_to[0], map_to[1], 0.0, max(Fc_list[1:]))) for f in Fc_list]

    for index in range(1,lines):
	tth = tth_list[index]
	Fc = y_list[index]
	hkl_label = str(int(h_list[index])) + str(int(k_list[index])) + str(int(l_list[index]))
	plt.plot((tth, tth),(Fc[0],Fc[1]), '-', c=line_color)
	if hkl_text:
	    plt.text(float(tth-0.8), float(Fc[1]+0.013), hkl_label)
	if labeled == False:
	    labeled = True
	    plt.plot(tth,Fc[1], marker_style, label=label, c=line_color)
	else:
	    plt.plot(tth,Fc[1], marker_style, c=line_color)

def plot_hkl(hkl_data, table, line_style = '--', line_color = 'red', hkl_text = False, 
	     map_to=None):
    lines = len(hkl_data[0])
    tth_index = table['2-theta']
#Fc_index = table['I100']
    Fc_index = table['Fc**2']
    tth_list = hkl_data[tth_index]
    Fc_list = hkl_data[Fc_index]

    if map_to is None:
	y_list = [(0.0, f) for f in Fc_list]
    else:
	y_list = [(map_to[0], map_value(f, map_to[0], map_to[1], 0.0, max(Fc_list[1:]))) for f in Fc_list]

    for index in range(1,lines):
	tth = tth_list[index]
	Fc = y_list[index]
	plt.plot((tth,tth),(Fc[0],Fc[1]), line_style)

data_list = []
data_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_00.xy', label = 'UO${}_2$-0,0wt%Er${}_2$O${}_3$'),
		  load_data_hkl('data/uo2.hkl')))
data_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_10.xy', label = 'UO${}_2$-1,0wt%Er${}_2$O${}_3$'),
		  load_data_hkl('data/uo2_10.hkl')))
data_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_25.xy', label = 'UO${}_2$-2,5wt%Er${}_2$O${}_3$'),
		  load_data_hkl('data/uo2_25.hkl')))
data_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_40.xy', label = 'UO${}_2$-4,0wt%Er${}_2$O${}_3$'),
		  load_data_hkl('data/uo2_40.hkl')))
data_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_98.xy', label = 'UO${}_2$-9,8wt%Er${}_2$O${}_3$'),
		  load_data_hkl('data/uo2_98.hkl')))
data2_list = []
data2_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_00.xy', label = 'UO2'),
		  load_data_hkl('data/uo2_inb.hkl')))
data2_list.append((DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/UO2_00.xy', label = 'Er2O3'),
		  load_data_hkl('data/er2o3.hkl')))

data00 = data_list[0][0]
data10 = data_list[1][0]
data25 = data_list[2][0]
data40 = data_list[3][0]
data98 = data_list[4][0]
data_er2o3_pwd = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/Er2O3.xy', label='Er2O3 PWD')
data_er2o3_dta = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/Er2O3_DTA.xy', label='Er2O3 DTA')
data_er2o3_nan = DataXY(file_path = '/home/alberto/Dropbox/XRD/Patterns/Er2O3nano.xy', label='Er2O3 NANO')
#data_er2o3_pwd.plot_data(map_to=(0.0,0.4), x_range = (), style='-', label = data_er2o3_pwd.label, color=None)
#data_er2o3_dta.plot_data(map_to=(0.2,0.6), x_range = (), style='-', label = data_er2o3_dta.label, color=None)
#data_er2o3_nan.plot_data(map_to=(0.4,0.8), x_range = (), style='-', label = data_er2o3_nan.label, color=None)
#data_er3o3_pwd.plot_data(map_to=(0.0,1.0), x_range = (), style='-', label = True, color=None)
#plt.legend()
#plt.show()


#exit()

if False: #NOTE: comparacao entre pastilhas 00, 10 e 25
    plt.figure(1)

    fig = plt.subplot(121)

    tth_from = 54.75
    tth_to = 60.15
    ax = plt.axis([tth_from,tth_to,0.0,1.0+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    plt.xlabel(r'2$\theta$, degrees')
    plt.ylabel('Intensity, arbitrary units')
    data25.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-2.5wt%Er${}_2$O${}_3$', color=color25)
    data10.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-1.0wt%Er${}_2$O${}_3$', color=color10)
    data00.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='-',  label=r'UO${}_2$-0.0wt%Er${}_2$O${}_3$', color=color00)
    plt.text(tth_from + 0.1, 1.0 - 0.1, '(a)', size=14)

    plt.legend()

    fig = plt.subplot(122)

#tth_from = 74.5
#tth_to = 80.5
    ax = plt.axis([tth_from,tth_to,0.0,1.0+0.01])


    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    plt.xlabel(r'2$\theta$, degrees')
    plt.ylabel('Intensity, arbitrary units')

    data98.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-9.8wt%Er${}_2$O${}_3$',color=color98)
    data40.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-4.0wt%Er${}_2$O${}_3$',color=color40)
    data00.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='-', label=r'UO${}_2$-0.0wt%Er${}_2$O${}_3$',color=color00)
    plt.text(tth_from + 0.1, 1.0 - 0.1, '(b)', size=14)

    plt.legend()

    plt.show()
    exit()

if False:
    plt.figure(1)

    fig = plt.subplot(221)

    tth_from = 54.75
    tth_to = 60.15
    ax = plt.axis([tth_from,tth_to,0.0,1.0+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    plt.xlabel(r'2$\theta$, degrees')
    plt.ylabel('Intensity, arbitrary units')
    data25.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$', color=color25)
    data10.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$', color=color10)
    data00.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='-',  label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$', color=color00)

    plt.legend()

    fig = plt.subplot(222)

    tth_from = 74.5
    tth_to = 79.5
    ax = plt.axis([tth_from,tth_to,0.0,1.0+0.01])


    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])

    data25.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color25)
    data10.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color10)
    data00.plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='-', label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',color=color00)

    fig = plt.subplot(223)
    plt.xlabel(r'2$\theta$, degrees')
    plt.ylabel('Intensity, arbitrary units')

    tth_from = 54.75
    tth_to = 60.15
    ax = plt.axis([tth_from,tth_to,0.0,1.0+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    data_list[4][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-9,8wt%Er${}_2$O${}_3$',color=color98)
    data_list[3][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-4,0wt%Er${}_2$O${}_3$',color=color40)
#data_list[2][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color25)
#data_list[1][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color10)
    data_list[0][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',color=color00)

    plt.legend()

    fig = plt.subplot(224)

    tth_from = 75.0
    tth_to = 80.30
    ax = plt.axis([tth_from,tth_to,0.0,1.0+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    data_list[4][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='--', label=r'UO${}_2$-9,8wt%Er${}_2$O${}_3$',color=color98)
    data_list[3][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-4,0wt%Er${}_2$O${}_3$',color=color40)
#data_list[2][0].plot_data(map_to=(0.0+0.2, 1.0+0.2), x_range=(tth_from, tth_to), style='-', label=r'UO${}_2$-2,5wt%Er${}_2$O${}_3$',color=color25)
#data_list[1][0].plot_data(map_to=(0.0+0.1, 1.0+0.1), x_range=(tth_from, tth_to), style='.-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color10)
    data_list[0][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',color=color00)


    plt.show()

    exit()

if False: #comparacao entre picos de 00 e 10
    plt.figure(1)
    fig = plt.subplot(131)

    tth_from = 27.0
    tth_to = 30.0
    ax = plt.axis([tth_from,tth_to,0.0,1.4+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])

    data_list[1][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='.-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color10)
    data_list[0][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',color=color00)
#plt.legend()

    fig = plt.subplot(132)

    tth_from = 53.0
    tth_to = 60.0
    ax = plt.axis([tth_from,tth_to,0.0,1.4+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    data_list[1][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='.-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color10)
    data_list[0][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',color=color00)


    fig = plt.subplot(133)

    tth_from = 74.0
    tth_to = 80.0
    ax = plt.axis([tth_from,tth_to,0.0,1.4+0.01])

    fig = plt.gca()
    plt.yticks([])
    fig.axes.yaxis.set_ticklabels([])
    data_list[1][0].plot_data(map_to=(0.0+0.0, 1.0+0.0), x_range=(tth_from, tth_to), style='.-', label=r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',color=color10)
    data_list[0][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='+-', label=r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',color=color00)

    plt.show()

    exit()



#plt.figure(1)
#
#fig = plt.subplot(221)
#plt.yticks([])
#fig.axes.yaxis.set_ticklabels([])
#tth_from = 27.5
#tth_to = 29.5
#data_list[2][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='--', label=r'0,0wt%-Er${}_2$O${}_3$',color=color25)
#data_list[3][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='+-', label=r'0,0wt%-Er${}_2$O${}_3$',color=color40)
#data_list[4][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='x-', label=r'0,0wt%-Er${}_2$O${}_3$',color=color98)
#
#fig = plt.subplot(222)
#plt.yticks([])
#fig.axes.yaxis.set_ticklabels([])
#tth_from = 54.0
#tth_to = 60.0
#data_list[2][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='x-', label=r'0,0wt%-Er${}_2$O${}_3$',color=color25)
#data_list[3][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='.', label=r'0,0wt%-Er${}_2$O${}_3$',color=color40)
#data_list[4][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-Er${}_2$O${}_3$',color=color98)
#
#fig = plt.subplot(223)
#plt.yticks([])
#fig.axes.yaxis.set_ticklabels([])
#tth_from = 32.25
#tth_to = 34
#data_list[2][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-er${}_2$o${}_3$',color=color25)
#data_list[3][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-er${}_2$o${}_3$',color=color40)
#data_list[4][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-er${}_2$o${}_3$',color=color98)
#
#fig = plt.subplot(224)
#plt.yticks([])
#fig.axes.yaxis.set_ticklabels([])
#tth_from = 74.5
#tth_to = 81.5
#data_list[2][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-er${}_2$o${}_3$',color=color25)
#data_list[3][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-er${}_2$o${}_3$',color=color40)
#data_list[4][0].plot_data(map_to=(0.0, 1.0), x_range=(tth_from, tth_to), style='-', label=r'0,0wt%-er${}_2$o${}_3$',color=color98)
#
#plt.show()
##exit()



ax = plt.axis([20,90,-0.2,1.0])
fig = plt.gca()
plt.yticks([])
fig.axes.yaxis.set_ticklabels([])
plt.ylabel(r"Intensidade, unidades arbitr$\acute{\mathrm{a}}$arias")
plt.xlabel(r'2$\theta$, graus')

labels = [r'UO${}_2$-0,0wt%Er${}_2$O${}_3$',
	    r'UO${}_2$-1,0wt%Er${}_2$O${}_3$',
	    r'UO${}_2$-2,5wt%Er${}_2$O${}_3$',
	    r'UO${}_2$-4,0wt%Er${}_2$O${}_3$',
	    r'UO${}_2$-9,8wt%Er${}_2$O${}_3$']

for data_index in range(len(data_list)):
    y_min = 1.0*data_index/len(data_list)
    y_max = 1.0*(data_index + 1.0)/(len(data_list))
    data = data_list[data_index]
    data_xy = data[0]
    data_hkl = data[1]
#plt.text(78.2, (y_min+y_max)*0.5, data_xy.label, fontsize=12)
    plt.text(78.2, (y_min+y_max)*0.5, labels[data_index], fontsize=12)
    data_xy.plot_data(map_to=(y_min, y_max), style='-')
    data_plot = data_xy.plot

hkl_map_to = (-1.0/len(data_list), 0.0*(-1 + 1.0)/len(data_list) -0.04)

plt.text(22.0,(hkl_map_to[0] + hkl_map_to[1])*0.5 + 0.025, r'UO${}_2$', fontsize=12)
plt.text(22.0,(hkl_map_to[0] + hkl_map_to[1])*0.5 - 0.025, r'Er${}_2$O${}_3$', fontsize=12)
plt.plot(21.5,(hkl_map_to[0] + hkl_map_to[1])*0.5 + 0.025 + 0.01325, 'rs')
plt.plot(21.5,(hkl_map_to[0] + hkl_map_to[1])*0.5 - 0.025 + 0.01325, 'bd')

plot_hkl_marker(data2_list[0][1][0], data2_list[0][1][1], marker_style = 's', line_color = 'red', hkl_text = True, map_to = hkl_map_to, label='UO2')
plot_hkl_marker(data2_list[1][1][0], data2_list[1][1][1], marker_style = 'd', line_color = 'blue', hkl_text = False, map_to = hkl_map_to, label='Er2O3')



plt.show()


