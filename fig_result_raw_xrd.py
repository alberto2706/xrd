#import draw_tools as drt
import xrd_tools as xrt
import matplotlib.pyplot as plt

xrd_data = lambda file_name : '/home/alberto/Dropbox/XRD/Patterns/{}'.format(file_name)

data_er2o3 = xrt.DataXY(file_path = xrd_data('Er2O3.xy'), label='Er2O3 PWD')
data_uo2 = xrt.DataXY(file_path = xrd_data('UO2-INB.xy'), label='Er2O3 PWD')

fig = plt.figure()

#plt.subplot(111)
ax = fig.gca()
ax.set_xlim(12,90)
plt.yticks([])
plt.xlabel(r'$2\theta$, graus')
plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')
#plt.text(80*0.8, max(data_er2o3.y)*0.8, '(a)', size=12)
#plt.title('(a)')
plt.plot(data_er2o3.x, data_er2o3.y,'--',label=r'Er${}_2$O${}_3$')
plt.plot(data_uo2.x, data_uo2.y,'-',label=r'UO${}_2$')

plt.legend()
plt.show()

exit()

plt.subplot(212)
ax = fig.gca()
ax.set_xlim(10,90)
plt.yticks([])
plt.xlabel(r'$2\theta$, graus')
plt.ylabel(r'Intensidade, unidades arbitr$\acute{\mathrm{a}}$rias')
plt.text(90*0.8, max(data_uo2.y)*0.8, '(b)', size=12)
#plt.title('(b)')
plt.plot(data_uo2.x, data_uo2.y)


#plt.legend()
plt.show()
