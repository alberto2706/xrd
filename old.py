#TODO: - cif
import math
import cromermann as cm
import matplotlib.pyplot as plt
from sys import argv

arguments = argv

wavelength_Cu_alpha_1 = 1.540562
wavelength_Cu_alpha_2 = 1.54439
wavelength_Cu = (2.0*wavelength_Cu_alpha_1 + wavelength_Cu_alpha_2)/3.0

global_U = [[0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0]]

global_U2 = [[0.01, 0.00, 0.00],
             [0.00, 0.01, 0.00],
             [0.00, 0.00, 0.01]]

class HKL(object):
    def __init__(self, h = 0, k = 0, l = 0):
	self.h = h
	self.k = k
	self.l = l

    def all_per(self): #TODO: h->k, k->l, l->h, etc.
	result = []
	h = self.h
	k = self.k
	l = self.l
	result.append(HKL(+h, +k ,+l))
	result.append(HKL(-h, +k ,+l))
	result.append(HKL(+h, -k ,+l))
	result.append(HKL(+h, +k ,-l))
	result.append(HKL(-h, -k ,+l))
	result.append(HKL(+h, -k ,-l))
	result.append(HKL(-h, +k ,-l))
	result.append(HKL(-h, -k ,-l))
	return result

    def diff_per(self):
	temp_result = self.all_per()
	result = []
	for hkl in temp_result:
	    if not hkl in result:
		result.append(hkl)
	return result

    def equiv_per(self, Q):
	pass

    def increment(self):
	indices = [self.h, self.k, self.l]
	if indices[0] == indices[1] and indices[1] == indices[2]:
	    self.h += 1
	    self.k = 0
	    self.l = 0
	elif indices[1] == indices[2]:
	    self.k += 1
	    self.l = 0
	else:
	    self.l += 1

    def __eq__(self, other):
	if self.h == other.h and self.k == other.k and self.l == other.l:
	    return True
	else:
	    return False

    def __ne__(self, other):
	if self == other:
	    return False
	else:
	    return True

    def __str__(self):
	m_s = '-'
	if self.h >= 0:
	    h_str = ' %s' % (str(self.h))
	else:
	    h_str = '%s%s' % (m_s, str(-self.h))
	if self.k >= 0:
	    k_str = ' %s' % (str(self.k))
	else:
	    k_str = '%s%s' % (m_s, str(-self.k))
	if self.l >= 0:
	    l_str = ' %s' % (str(self.l))
	else:
	    l_str = '%s%s' % (m_s, str(-self.l))
	result = '%s%s%s' % (h_str, k_str, l_str)
	return result

class Vector(object):
    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
	self.x = x
	self.y = y
	self.z = z

    def length(self):
	result = 0.0
	result += self.x*self.x
	result += self.y*self.y
	result += self.z*self.z
	return math.sqrt(result)

    def __add__(self, other):
	result = Vector()
	result.x = self.x + other.x
	result.y = self.y + other.y
	result.z = self.z + other.z
	return result

    def __sub__(self, other):
	result = Vector()
	result.x = self.x - other.x
	result.y = self.y - other.y
	result.z = self.z - other.z
	return result

    def __mul__(self, other): #prod escalar
	if type(other) == float or type(other) == int:
	    result = Vector()
	    result.x = self.x*other
	    result.y = self.y*other
	    result.z = self.z*other
	elif type(other) == Vector:
	    result = 0.0
	    result += self.x*other.x
	    result += self.y*other.y
	    result += self.z*other.z
	else:
	    exit(1)
	return result

    def __xor__(self, other): #prod vetorial
	result = Vector()
	result.x = self.y*other.z - self.z*other.y
	result.y = self.z*other.x - self.x*other.z
	result.z = self.x*other.y - self.y*other.x
	return result

    def __str__(self):
	result = '[%f, %f, %f]' % (self.x, self.y, self.z)
	return result

class Matrix(object):
    def __init__(self, *kwarg):
	pass

class Atom(object):
    def __init__(self, type = 'H', pos = Vector()):
	self.type = type
	self.label = type
	self.pos = pos

    def form_factor(self, Q):
	Q_over_4pi = Q/(4.0*math.pi)
	cm_coeff = cm.coeff[self.type]
        a1 = cm_coeff[0]
        a2 = cm_coeff[1]
        a3 = cm_coeff[2]
        a4 = cm_coeff[3]
        b1 = cm_coeff[5]
        b2 = cm_coeff[6]
        b3 = cm_coeff[7]
        b4 = cm_coeff[8]
        c = cm_coeff[4]
        
        result = 0.0
        result += a1*math.exp(-b1*math.pow(Q_over_4pi, 2.0))
        result += a2*math.exp(-b2*math.pow(Q_over_4pi, 2.0))
        result += a3*math.exp(-b3*math.pow(Q_over_4pi, 2.0))
        result += a4*math.exp(-b4*math.pow(Q_over_4pi, 2.0))
        result += c
        return result

class Base(object):
    def __init__(self, a = Vector(1.0, 0.0, 0.0),
		       b = Vector(0.0, 1.0, 0.0),
		       c = Vector(0.0, 0.0, 1.0)):
	self.a = a
	self.b = b
	self.c = c

    def reciprocal(self):
	result = Base()
	V = self.a*(self.b^self.c)
	result.a = (self.b^self.c)*(2.0*math.pi/V)
	result.b = (self.c^self.a)*(2.0*math.pi/V)
	result.a = (self.c^self.b)*(2.0*math.pi/V)
	#result.a = cross(self.b, self.c)*(2.0*math.pi/V)
	#result.b = cross(self.c, self.a)*(2.0*math.pi/V)
	#result.c = cross(self.a, self.b)*(2.0*math.pi/V)
	return result

    def __str__(self):
	result = '([%f,%f,%f],\n [%f,%f,%f],\n [%f,%f,%f])' % \
		  (self.a.x, self.a.y, self.a.z, 
		  self.b.x, self.b.y, self.b.z, 
		  self.c.x, self.c.y, self.c.z)
	return result

class Crystal(object):
    def __init__(self, atoms = [], base = Base(), system = 'cubic',
                       aniso = global_U):
	self.atoms = atoms
	self.base = base
	self.system = system
	self.aniso = aniso

def debye_waller_factor(crystal, hkl):
    result = 0.0

    h = hkl.h
    k = hkl.k
    l = hkl.l
    U = crystal.aniso
    base_r = crystal.base.reciprocal()

    exp_arg = 0.0
    exp_arg += U[0][0]*(h*h)*(base_r.a.length()*base_r.a.length())
    exp_arg += U[1][1]*(k*k)*(base_r.b.length()*base_r.b.length())
    exp_arg += U[2][2]*(l*l)*(base_r.c.length()*base_r.c.length())
    exp_arg += U[0][1]*(h*k)*(base_r.a.length()*base_r.b.length())
    exp_arg += U[0][2]*(h*l)*(base_r.a.length()*base_r.c.length())
    exp_arg += U[1][2]*(k*l)*(base_r.b.length()*base_r.c.length())
    exp_arg += U[1][0]*(k*h)*(base_r.b.length()*base_r.a.length())
    exp_arg += U[2][0]*(l*h)*(base_r.c.length()*base_r.a.length())
    exp_arg += U[2][1]*(l*k)*(base_r.c.length()*base_r.b.length())

    #result = math.exp(-2.0*(math.pi**2)*exp_arg)
    result = math.exp(-exp_arg)
    return result

def dot(u, v):
    result = 0.0
    result += u.x*v.x
    result += u.y*v.y
    result += u.z*v.z
    return result

def cross(u, v):
    result = Vector()
    result.x = u.y*v.z - u.z*v.y
    result.y = u.z*v.x - u.x*v.z
    result.z = u.x*v.y - u.y*v.x
    return result

def load_cif(cif_path):
    result = {}
    result['comments'] = []
    file_handle = open(cif_path, 'r')
    file_string = file_handle.read()
    file_handle.close()
    separators = [' ', '\t', '\n']
    delimiters = ['\'', ';', '\"']
    buffer = ''
    loop_data_names = []
    loop_data_values = []
    in_quotes = False

    index = 0
    while index < len(file_string):
	c = file_string[index]
	if c in delimiters:
	    in_quotes = not in_quotes
	elif in_quotes:
	    buffer += c
	elif c in separators:
	    if buffer == '':
		pass
	    elif buffer[0] == '#':
		if c == '\n':
		    buffer = buffer.strip('#')
		    if buffer:
			if buffer[0] == ' ':
			    buffer = buffer[1:]
			result['comments'].append(buffer)
			buffer = ''
		else:
		    buffer += c
	    elif buffer[0] == '_':
		loop_data_names.append(buffer)
		buffer = ''
	    elif buffer[:5] == 'data_':
		loop_data_names.append(buffer.split('_')[0])
		loop_data_values.append(buffer.split('_')[1])
		buffer = ''
	    elif buffer[:5] == 'loop_':
		loop_data_names = []
		loop_data_values = []
		buffer = ''
	    else:
		data_index = len(loop_data_names) - 1
		data_name = loop_data_names[data_index]
		if not data_name in result:
		    result[data_name] = []
		result[data_name].append(buffer)
		buffer = ''
	else:
	    buffer += c
	index += 1
    #TODO: result_ex
    return result

def eval_F2(Q, crystal):
    result = 0.0
    F_Re   = 0.0
    F_Im   = 0.0
    Q_len = Q.length()
    for atom in crystal.atoms:
	f     = atom.form_factor(Q_len)
	r     = atom.pos
	#r_Q   = dot(r, Q)*5.45
	r_Q   = (r*Q)*5.45
	F_Re += f*math.cos(r_Q)
	F_Im += f*math.sin(r_Q)
    result = F_Re**2 + F_Im**2
    return result

def get_theta_rad(Q, wl):
    result = 0.0
    try:
	Q = Q.length()
    except:
	Q = Q
    result = math.asin(wl*Q/(4.0*math.pi))
    return result

def lorentz_factor(Q, wl):
    ttheta = 2.0*get_theta_rad(Q, wl)
    result = (1.0 + math.cos(ttheta))/(math.sin(ttheta*0.5)**2 *
				       math.cos(ttheta*0.5))
    return result

def eval_Q_cubic(crystal, hkl):
    result = Vector()
    result.x = hkl.h
    result.y = hkl.k
    result.z = hkl.l
    a = crystal.base.a.length()
    result = result*(2.0*math.pi/a)
    return result

def eval_hkl_max(crystal, wl):
    result = HKL()
    base = crystal.base
    result.h = int(2.0*base.a.length()/wl)
    result.k = int(2.0*base.b.length()/wl)
    result.l = int(2.0*base.c.length()/wl)
    #TODO: cubic
    return result

def peak_hist(crystal, wl):
    result = {}
    Q_max = 4.0*math.pi/wl
    hkl_max = eval_hkl_max(crystal, wl)
    hkl = HKL(1, 0, 0)
    while hkl != hkl_max:
	m = len(hkl.diff_per()) #NOTE: cubic.
	Q = eval_Q_cubic(crystal, hkl)
	try:
	    LP = lorentz_factor(Q, wl)
	except:
	    LP = 1.0
	if Q.length() <= Q_max:
	    F2 = eval_F2(Q, crystal)
	    DW = debye_waller_factor(crystal, hkl)
	    result[Q.length()] = {}
	    result[Q.length()]['F2'] = F2
	    result[Q.length()]['m'] = m
	    result[Q.length()]['LP'] = LP
	    result[Q.length()]['DW'] = DW
	    result[Q.length()]['tth'] = get_theta_rad(Q, wl)
	    result[Q.length()]['hkl'] = HKL(hkl.h, hkl.k, hkl.l)
	    #print hkl, F2*LP*m
	else:
	    pass
	hkl.increment()
    return result

def print_hist(hist, wl = None, peak_label = False, 
	       hist_color = None, hist_width = None):
    I_max = 1.0
    normalize = True
    if normalize:
	for q in hist:
	    I = hist[q]['F2']*hist[q]['m']*hist[q]['LP']*hist[q]['DW']
	    if I > I_max:
		I_max = I

    if not hist_color:
	hist_color = 'blue'
    if not hist_width:
	hist_width = 2.0
 
    for q in hist:
	I = hist[q]['F2']*hist[q]['m']*hist[q]['LP']*hist[q]['DW']/I_max
	if I > 0.01:
	    if wl:
		x = 2.0*get_theta_rad(q, wl)*180.0/math.pi
	    else:
		x = q
	    plt.plot((x, x), (0,I), color = hist_color, linewidth = hist_width)
	    if peak_label:
		plt.annotate(s = hist[q]['hkl'], xy=(x,I))

def cif_split(string):
    result = []
    try:
	a = string.index('\'')
	b = string.index('\'', a + 1)
    except:
	result = string.split(' ')
	return result
    temp_string = string[:a+1] + '*'*(b-a) + string[b:]
    temp_result = temp_string.split(' ')
    for entry in temp_result:
	if '\'' in entry:
	    a_temp = entry.index('\'')
	    b_temp = entry.index('\'', a_temp + 1)
	    new_string = entry[:a_temp] + string[a+1:b] + entry[b_temp+1:]
	    result.append(new_string)
	else:
	    result.append(entry)
    while '' in result:
	result.remove('')
    return result

def debye_factor(Q, crystal):
    result = 0.0
    for a in range(len(crystal.atoms)):
	for b in range(len(crystal.atoms)):
	    if a == b:
		pass
	    else:
		atom_a = crystal.atoms[a]
		atom_b = crystal.atoms[b]
		f_a = atom_a.form_factor(Q)
		f_b = atom_b.form_factor(Q)
		r_ab = (atom_a.pos - atom_b.pos).length()
		if Q == 0:
		    result += f_a*f_b
		else:
		    result += f_a*f_b*math.sin(Q*r_ab)/(Q*r_ab)
		    result += f_a*f_b
    return result


def main():

    uo2 = Crystal()
    uo2.atoms = [Atom('U', Vector(0.0, 0.0, 0.0)),
		 Atom('U', Vector(0.5, 0.5, 0.0)),
	 	 Atom('U', Vector(0.0, 0.5, 0.5)),
		 Atom('U', Vector(0.5, 0.0, 0.5)),
		 Atom('O', Vector(0.25, 0.25, 0.25)),
		 Atom('O', Vector(0.75, 0.25, 0.25)),
		 Atom('O', Vector(0.25, 0.75, 0.25)),
		 Atom('O', Vector(0.25, 0.25, 0.75)),
		 Atom('O', Vector(0.25, 0.75, 0.75)),
		 Atom('O', Vector(0.75, 0.75, 0.25)),
		 Atom('O', Vector(0.75, 0.25, 0.75)),
		 Atom('O', Vector(0.75, 0.75, 0.75))]
    a_UO2 = 5.46
    uo2.base = Base(Vector(a_UO2, 0.0, 0.0),
		    Vector(0.0, a_UO2, 0.0),
		    Vector(0.0, 0.0, a_UO2))

    wl = wavelength_Cu
    hist = peak_hist(uo2, wl)

#uo2.aniso = global_U2
#hist2 = peak_hist(uo2, wl)
    print_hist(hist, wl, peak_label = True)
#print_hist(hist2, wl, hist_color = 'red')
    plt.show()

main() 
