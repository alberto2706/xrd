import matplotlib.pyplot as plt
from math import *

global_wl = 1.54051

class Angle(object):
    def __init__(self, theta = 0.0, mode = 'd'):
	self.d = 0.0
	self.r = 0.0
	if mode == 'd':
	    self.d = theta
	    self.r = radians(theta)
	else:
	    self.d = degrees(theta)
	    self.r = theta
    
    def __str__(self):
	return "%f(%frad)" % (self.d, self.r)

class HKL(object):
    def __init__(self, h=1, k=0, l=0):
	self.h = h
	self.k = k
	self.l = l
    def __str__(self):
	return "(%d,%d,%d)" % (self.h, self.k, self.l)

def eval_tth(a, hkl, mode = 'd', wl = global_wl):
    h = hkl.h
    k = hkl.k
    l = hkl.l
    sqrt_hkl = sqrt(h**2 + k**2 + l**2)
    asin_arg = (wl/(2.0*a))*sqrt_hkl
    tth = 2.0*asin(asin_arg)
    result = Angle(tth, 'r')
    return result

def eval_a(tth, hkl, wl = global_wl):
    h = hkl.h
    k = hkl.k
    l = hkl.l
    sin_th = sin(0.5*tth.r)
    sqrt_hkl = sqrt(h**2 + k**2 + l**2)
    result = wl*sqrt_hkl/(2.0*sin_th)
    return result

def eval_a_from_x(x, work='y'):
    result = 0.0
    if work == 'y':
	result = 0.54695 - (0.02704*x)
    elif work == 'k':
	result = 0.5471 - (0.0264*x)
    else:
	result = -1
	exit(11)
    return (10.0*result)

def eval_x_from_a(a, work='y'):
    result = 0.0
    a /= 10.
    if work == 'y':
	result = (0.54695- a)/(0.02704)
    elif work == 'k':
	result = (0.5471 - a)/(0.0264)
    else:
	result = -1
    return result

def eval_n(wt_Er2O3):
    A_O = 15.999
    A_U = 238.02
    A_Er = 167.26
    A_UO2 = A_U + 2.0*A_O
    A_Er2O3 = 2.0*A_Er + 3.0*A_O

    M_pellet = 15.60/3.0 #g
    
    C_Er2O3 = wt_Er2O3*0.01
    C_ADS = 0.02
    C_UO2 = (1.0 - C_Er2O3 - C_ADS)

    M_UO2 = M_pellet*C_UO2
    M_Er2O3 = M_pellet*C_Er2O3

    N_A = 6.02E23

    n_UO2 = (M_UO2/A_UO2)*N_A
    n_Er2O3 = (M_Er2O3/A_Er2O3)*N_A

    return n_UO2, (2.0*n_Er2O3)

def eval_x(wt_Er2O3):
    A_O = 15.999
    A_U = 238.02
    A_Er = 167.26
    A_UO2 = A_U + 2.0*A_O
    A_Er2O3 = 2.0*A_Er + 3.0*A_O

    M_pellet = 15.60/3.0 #g
    
    C_Er2O3 = wt_Er2O3*0.01
    C_ADS = 0.02
    C_UO2 = (1.0 - C_Er2O3 - C_ADS)

    M_UO2 = M_pellet*C_UO2
    M_Er2O3 = M_pellet*C_Er2O3

    N_A = 6.02E23

    n_UO2 = (A_UO2/M_UO2)*N_A
    n_Er2O3 = (A_Er2O3/M_Er2O3)*N_A

    return (n_UO2/(2.0*n_Er2O3))

def est_a(tth, hkl):
    result = 0.0
    h = hkl[0]
    k = hkl[1]
    l = hkl[2]
    wl = 1.54051
    
    result = wl/(2.0*sin(0.5*radians(tth)))
    result = result*sqrt(h**2 + k**2 + l**2)

    return(result)

def main(a_arg):
    a = 5.44 #angstrom
    a = a_arg
    hkl = (2,0,0)

    h = hkl[0]
    k = hkl[1]
    l = hkl[2]

    wl = 1.54051

    arg = (wl**2)*(h**2 + k**2 + l**2)/(4.0*(a**2))

    tth = 2.0*degrees(sqrt(arg))
    return tth

def get_data(file_name):
    data_dir_path = '/home/alberto/Dropbox/Notes/Tese/scripts/data/'
    input_file_handle = open(data_dir_path + file_name, 'r')
    input_file_string = input_file_handle.read()
    input_file_handle.close()
    
    x, y = [], []
    file_data_lines = input_file_string.split('\n')
    print('file_name: ', file_name)

    for line in file_data_lines:
	print(line),
	if(line == ''):
	    sep = ''
	elif(' ' in line):
	    sep = ' '
	elif('\t' in line):
	    sep = '\t'
	else:
	    print('nenhum separador.\nlinha: '),
	    print(line)
	    sep = ''
	if sep == '':
	    pass
	else:
	    line_data = line.split()
	    print('line_data:',line_data)
	    x.append(float(line_data[0]))
	    y.append(float(line_data[1]))

    y_max = max(y)

    output_file_handle = open(data_dir_path + 'out_' + file_name, 'w')
    output_file_string = ''
    for i in xrange(len(x)):
	output_file_string += '%f\t%f\n' % (x[i], 100*y[i]/y_max)

    output_file_handle.write(output_file_string)
    output_file_handle.close()

#x = eval_x(2.5)
#print "x(2,5%) = ", x

#a_yamanaka = eval_a_from_x(x, 'y')
#a_kim = eval_a_from_x(x, 'k')
#print "a_yamanaka = ", a_yamanaka
#print "a_kim = ", a_kim 

#print "tth_a_yamanaka = ", eval_tth(a_yamanaka, HKL(2, 0, 0))
#print "tth_a_kim = ", eval_tth(a_kim, HKL(2, 0, 0))

#print "a(tth=32.8) = ", eval_a(Angle(32.8), HKL(2,0,0))

#hkl = HKL(4, 2, 2)
#tth = 87.323
#a = eval_a(Angle(tth), hkl)
#print(a)

x_10 = eval_x(9.8)
print x_10
###int eval_a_from_x(x_10)
exit()

x_list = []
y_kim_list = []
y_yam_list = []
y_a_list = []
y_b_list = []
y_m_list = []

x=0.0
x_list.append(x)
y_kim_list.append(eval_a_from_x(x, 'k'))
y_yam_list.append(eval_a_from_x(x, 'y'))
UO2 = 5.47121
UO2_a= 5.47121
UO2_b= 5.47121
a_m= 5.47121
y_a_list.append(UO2_a)
y_b_list.append(UO2_b)
y_m_list.append(a_m)

x = eval_x(1.0)
x_list.append(x)
y_kim_list.append(eval_a_from_x(x, 'k'))
y_yam_list.append(eval_a_from_x(x, 'y'))
UO2_a= 5.46997# 0.82
UO2_b= 5.45364# 0.18
a_m= 5.4670306
y_a_list.append(UO2_a)
y_b_list.append(UO2_b)
y_m_list.append(a_m)


x = eval_x(2.5)
x_list.append(x)
y_kim_list.append(eval_a_from_x(x, 'k'))
y_yam_list.append(eval_a_from_x(x, 'y'))
UO2_a= 5.47159# 0.63
UO2_b= 5.44651# 0.37
a_m= 5.4623104
y_a_list.append(UO2_a)
y_b_list.append(UO2_b)
y_m_list.append(a_m)

x = eval_x(4.0)
x_list.append(x)
y_kim_list.append(eval_a_from_x(x, 'k'))
y_yam_list.append(eval_a_from_x(x, 'y'))
UO2_a= 5.47057# 0.692
UO2_b= 5.43885# 0.308
a_m= 5.46080024
y_a_list.append(UO2_a)
y_b_list.append(UO2_b)
y_m_list.append(a_m)

x = eval_x(9.8)
x_list.append(x)
y_kim_list.append(eval_a_from_x(x, 'k'))
y_yam_list.append(eval_a_from_x(x, 'y'))
UO2_a= 5.47221# 0.284
UO2_b= 5.42151# 0.716
a_m= 5.4359088
y_a_list.append(UO2_a)
y_b_list.append(UO2_b)
y_m_list.append(a_m)

plt.plot(x_list, y_kim_list, '--', label='kim')
plt.plot(x_list, y_yam_list, '--', label='yam')
plt.plot(x_list, y_a_list, 'o', label='a')
plt.plot(x_list, y_b_list, 'o', label='b')
plt.plot(x_list, y_m_list, '*', label='m')
plt.legend()
plt.show()
