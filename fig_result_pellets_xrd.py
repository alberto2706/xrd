#import draw_tools as drt
import xrd_tools as xrt
import matplotlib.pyplot as plt

xrd_data = lambda file_name : '/home/alberto/Dropbox/XRD/Patterns/{}'.format(file_name)

data_00 = xrt.DataXY(file_path = xrd_data('UO2_00.xy'), label='00')
data_10 = xrt.DataXY(file_path = xrd_data('UO2_10.xy'), label='10')
data_25 = xrt.DataXY(file_path = xrd_data('UO2_25.xy'), label='25')
data_40 = xrt.DataXY(file_path = xrd_data('UO2_40.xy'), label='40')
data_98 = xrt.DataXY(file_path = xrd_data('UO2_98.xy'), label='98')

fig = plt.figure()
#plt.subplot(211)
ax = fig.gca()
#ax.set_xlim(12,80)
plt.yticks([])

#plt.plot(data_00.x, data_00.y)
#plt.plot(data_98.x, data_98.y)
tth_range = (20.0, 90.0)
data_00.plot_data(map_to=(0,1), x_range = tth_range, style='-', label = None, color=None)
data_10.plot_data(map_to=(1,2), x_range = tth_range, style='-', label = None, color=None)
data_25.plot_data(map_to=(2,3), x_range = tth_range, style='-', label = None, color=None)
data_40.plot_data(map_to=(3,4), x_range = tth_range, style='-', label = None, color=None)
data_98.plot_data(map_to=(4,5), x_range = tth_range, style='-', label = None, color=None)



plt.xlabel(r'$2\theta$, graus')
plt.ylabel(r'Intensidade, unidades arbitr$\mathrm{\'a}$rias')
plt.show()

fig = plt.figure(1)

plt.subplot(131)
plt.title('111')
plt.yticks([])
tth_range = (28.0, 29.2)
data_00.plot_data(map_to=(0.0,0.4), x_range = tth_range, style='-', label = None, color=None)
plt.text(29.0, 0.0+0.05, '0.0%')
data_10.plot_data(map_to=(0.2,0.6), x_range = tth_range, style='-', label = None, color=None)
plt.text(29.0, 0.2+0.05, '1.0%')
data_25.plot_data(map_to=(0.4,0.8), x_range = tth_range, style='-', label = None, color=None)
plt.text(29.0, 0.4+0.05, '2.5%')
data_40.plot_data(map_to=(0.6,1.0), x_range = tth_range, style='-', label = None, color=None)
plt.text(29.0, 0.6+0.05, '4.0%')
data_98.plot_data(map_to=(0.8,1.2), x_range = tth_range, style='-', label = None, color=None)
plt.text(29.0, 0.8+0.05, '9.8%')
#plt.ylabel(r'Intensidade, unidades arbitr$\mathrm{\'a}$rias')
plt.ylabel(r'Intensity, arbitrary units')

plt.subplot(132)
plt.title('220')
plt.yticks([])
tth_range = (46.8, 48.0)
data_00.plot_data(map_to=(0.0,0.4), x_range = tth_range, style='-', label = None, color=None)
data_10.plot_data(map_to=(0.2,0.6), x_range = tth_range, style='-', label = None, color=None)
data_25.plot_data(map_to=(0.4,0.8), x_range = tth_range, style='-', label = None, color=None)
data_40.plot_data(map_to=(0.6,1.0), x_range = tth_range, style='-', label = None, color=None)
data_98.plot_data(map_to=(0.8,1.2), x_range = tth_range, style='-', label = None, color=None)
#plt.xlabel(r'$2\theta$, graus')
plt.xlabel(r'$2\theta$, degrees')

plt.subplot(133)
plt.title('311')
plt.yticks([])
tth_range = (75.5, 77.67)
data_00.plot_data(map_to=(0.0,0.4), x_range = tth_range, style='-', label = None, color=None)
data_10.plot_data(map_to=(0.2,0.6), x_range = tth_range, style='-', label = None, color=None)
data_25.plot_data(map_to=(0.4,0.8), x_range = tth_range, style='-', label = None, color=None)
data_40.plot_data(map_to=(0.6,1.0), x_range = tth_range, style='-', label = None, color=None)
data_98.plot_data(map_to=(0.8,1.2), x_range = tth_range, style='-', label = None, color=None)
plt.show()
