import matplotlib.pyplot as plt
import math

default_arrow_style = {}
default_arrow_style['ec'] = 'k'
default_arrow_style['fc'] = 'k'
default_arrow_style['head_width'] = 0.05/2.0
default_arrow_style['head_length'] = 0.1/2.0
fig = plt.figure()
ax = fig.gca()
ax.set_aspect('equal')
if False:
    plt.yticks([])
    plt.xticks([])
    ax.axis('off')
    #fig.patch.set_visible(False)

class Vector(object):
    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
	self.x = x
	self.y = y
	self.z = z

    def length(self):
	result = 0.0
	result += self.x*self.x
	result += self.y*self.y
	result += self.z*self.z
	return math.sqrt(result)

    def normalized(self):
	result = Vector()
	result = self*(1.0/self.length())
	return result

    def __add__(self, other):
	result = Vector()
	result.x = self.x + other.x
	result.y = self.y + other.y
	result.z = self.z + other.z
	return result

    def __sub__(self, other):
	result = Vector()
	result.x = self.x - other.x
	result.y = self.y - other.y
	result.z = self.z - other.z
	return result

    def __mul__(self, other): #prod escalar
	if type(other) == float or type(other) == int:
	    result = Vector()
	    result.x = self.x*other
	    result.y = self.y*other
	    result.z = self.z*other
	elif type(other) == Vector:
	    result = 0.0
	    result += self.x*other.x
	    result += self.y*other.y
	    result += self.z*other.z
	else:
	    exit(1)
	return result

    def __xor__(self, other): #prod vetorial
	result = Vector()
	result.x = self.y*other.z - self.z*other.y
	result.y = self.z*other.x - self.x*other.z
	result.z = self.x*other.y - self.y*other.x
	return result

    def __str__(self):
	result = '[%f, %f, %f]' % (self.x, self.y, self.z)
	return result

    def rotate(self,angle):
	th = angle*math.pi/180.0
	result = Vector()
	cos_th = math.cos(th)
	sin_th = math.sin(th)
	result.x = self.x*cos_th + self.y*sin_th
	result.y = -self.x*sin_th + self.y*cos_th
	return result

    def translate(self, shift):
	result = Vector()
	result = self + shift
	return result

def draw_line(V1, V2, shift = None, color = None):
    lc = 'k'
    if color:
	lc = color
    if shift is None:
	shift = Vector()

    if type(V1) == type(tuple()):
	V1 = Vector(V1[0], V1[1])
    if type(V2) == type(tuple()):
	V2 = Vector(V2[0], V2[1])
    if type(shift) == type(tuple()):
	shift = Vector(shift[0], shift[1])
	
    P1 = (V1.x + shift.x, V2.x + shift.x)
    P2 = (V1.y + shift.y, V2.y + shift.y)
    p = plt.plot(P1, P2, color=lc)

def draw_list(line_list, cycle = False, shift=None):
    P1 = line_list[0]
    for index in range(1,len(line_list)):
	P2 = line_list[index]
	draw_line(P1,P2,shift)
	P1 = line_list[index]
    if cycle:
	draw_line(P1, line_list[0],shift)

def draw_rect(pos, dim, shift = (0.0, 0.0)):
    if type(pos) == type(tuple()):
	pos = Vector(pos[0], pos[1])
    if type(dim) == type(tuple()):
	dim = Vector(dim[0], dim[1])
    if type(shift) == type(tuple()):
	shift = Vector(shift[0], shift[1])
    pos = pos + shift
    x_dim = Vector(dim.x, 0.0)
    y_dim = Vector(0.0, dim.y)
    draw_line(pos, pos + x_dim)
    draw_line(pos + x_dim, pos + dim)
    draw_line(pos + dim, pos + y_dim)
    draw_line(pos + y_dim, pos)

def draw_arrow(from_pos, to_pos, arrow_style = default_arrow_style):
    for arg in default_arrow_style:
	    if not arg in arrow_style:
		    arrow_style[arg] = default_arrow_style[arg]
    
    x1 = float(from_pos.x)
    y1 = float(from_pos.y)
    x2 = float(to_pos.x)
    y2 = float(to_pos.y)
    dx = x2 - x1
    dy = y2 - y1

    ax.arrow(x1,y1,dx,dy,**arrow_style)
    
def draw_wave(from_pos, to_pos, A = 1.0, n = 1):
    L = (to_pos - from_pos).length()
    k = (to_pos - from_pos)*(1.0/(to_pos-from_pos).length())
    draw_vec = from_pos
    print L
    print k
    print draw_vec
    x = 0.0
    x_vec = from_pos
    sin_f = math.sin(2.0*math.pi*float(n)*(x)/L)
    amplitude = Vector(k.x, k.y).rotate(90.0)
    amplitude = amplitude*A
    amplitude = amplitude*sin_f
    amplitude = amplitude + x_vec
    draw_vec = Vector(amplitude.x, amplitude.y)
    dx = L*0.01
    while x < L:
	x += dx
	x_vec = k*x
	x_vec = x_vec + from_pos
	sin_f = math.sin(2.0*math.pi*float(n)*(x)/L)
	amplitude = Vector(k.x, k.y).rotate(90.0)
	amplitude = amplitude*A
	amplitude = amplitude*sin_f
	amplitude = amplitude + x_vec
	new_draw_vec = Vector(amplitude.x, amplitude.y)
	draw_line(draw_vec, new_draw_vec)
	draw_vec = Vector(new_draw_vec.x, new_draw_vec.y)

def test():
#pos_from = Vector(0.,0.)
#pos_to = Vector(1.,0.)
#n = 12
#draw_wave(pos_from, pos_to, n)
#plt.show()
    d = 1.0
    theta  = 10.0 + 45.0

    L = 10.0
    wP = (L*0.5)/math.cos(math.radians(theta))
    wL = wP/3.0
    A = Vector(-(L*0.5), (L*0.5)/math.sin(math.radians(theta)))
    A = Vector(-8,10)
    u = A - Vector()
    u = u*(1.0/u.length())

    P = A + u*wL
    print A, P
    draw_wave(A, P, 1.0, 1)
    draw_line(P, Vector())
    

#draw_line((-L/2.0,0), (L/2.0, 0), shift=(0.0, -0.0*d))

#draw_line((-L/2.0,H),(0.0, 0), shift=(0.0, -0.0*d))
#draw_line((0.0,0.0), (L/2.0, H), shift=(0.0, -0.0*d))



#draw_line((0.0,0.0), (10.0, 0.0))
#draw_line((0.0,0.0), (10.0, 0.0), shift=(0.0, -1.0))
#draw_line((0.0,0.0), (10.0, 0.0), shift=(0.0, -2.0))
#
#draw_line((0.0,2.0), (5.0, 0.0), shift=(0.0, 0.0))
#draw_line((5.0,0.0), (10.0, 2.0), shift=(0.0, 0.0))

    plt.show()

def angle_between(u, v):
    result = math.degrees(math.acos((u*v)/(u.length()*v.length())))
    return result

#test()
