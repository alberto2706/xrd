from xrd_tools import *
import matplotlib.pyplot as plt

xy_data = DataXY('/home/alberto/Dropbox/XRD/Patterns/UO2_98.xy')

hkl_data_alpha, hkl_table_alpha = load_data_hkl('alpha.hkl')
hkl_data_beta, hkl_table_beta = load_data_hkl('beta.hkl')
hkl_data_rho, hkl_table_rho = load_data_hkl('rho.hkl')

fig = plt.figure()

plt.subplot(131)
xy_data.plot_data(map_to=(0.0,1.0), x_range = (), style='-', label = None,
	color=color98)

phases = load_hkl_from_csv('rho_np.csv')

print_hkl_phases(phases[1],bar_color=color10,map_to=(0.0,0.5),phase_label='Fase $\\alpha$',phase_marker='^')
print_hkl_phases(phases[2],bar_color=color25,map_to=(0.0,0.5),phase_label='Fase $\\beta$',phase_marker='v')
print_hkl_phases(phases[0],bar_color=color40,map_to=(0.0,0.18),phase_label='Fase $\\rho$',phase_marker='s')

plt.yticks([])
plt.ylabel('Intensidade, u. a.')
#plt.xlabel('$2\\theta$, graus')





plt.subplot(132)
xy_data.plot_data(map_to=(0.0,1.0), x_range = (), style='-', label = None,
	color=color98)

phases = load_hkl_from_csv('rho_np.csv')

print_hkl_phases(phases[1],bar_color=color10,map_to=(0.0,0.5/2.7),phase_label='Fase $\\alpha$',phase_marker='^')
print_hkl_phases(phases[2],bar_color=color25,map_to=(0.0,0.5/2.7),phase_label='Fase $\\beta$',phase_marker='v')
print_hkl_phases(phases[0],bar_color=color40,map_to=(0.0,0.18/1.3),phase_label='Fase $\\rho$',phase_marker='s')

plt.yticks([])
#plt.ylabel('Intensidade, u. a.')
plt.xlabel('$2\\theta$, graus')





plt.subplot(133)
xy_data.plot_data(map_to=(0.0,1.0), x_range = (), style='-', label = None,
	color=color98)

phases = load_hkl_from_csv('rho_np.csv')

print_hkl_phases(phases[1],bar_color=color10,map_to=(0.0,0.5/2.6),phase_label='Fase $\\alpha$',phase_marker='^')
print_hkl_phases(phases[2],bar_color=color25,map_to=(0.0,0.5/2.6),phase_label='Fase $\\beta$',phase_marker='v')
print_hkl_phases(phases[0],bar_color=color40,map_to=(0.0,0.18/2.6),phase_label='Fase $\\rho$',phase_marker='s')

plt.yticks([])
#plt.ylabel('Intensidade, u. a.')
#plt.xlabel('$2\\theta$, graus')



plt.legend()
plt.show()
exit()
