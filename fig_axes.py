import matplotlib.pyplot as plt
import draw_tools as dt
from mpl_toolkits.mplot3d import Axes3D
import math

rot_z = lambda x, v: dt.Vector(v.x*math.cos(x) + v.y*math.sin(x),
			       -v.x*math.sin(x) + v.y*math.cos(x),
			       v.z)
rot_y = lambda x, v: dt.Vector(v.x*math.cos(x) + v.z*math.sin(x),
			       v.y,
			       -v.x*math.sin(x) + v.z*math.cos(x))
rot_x = lambda x, v: dt.Vector(v.x,
			       v.y*math.cos(x) + v.z*math.sin(x),
			       -v.y*math.sin(x) + v.z*math.cos(x))
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_aspect('equal')

arrow_dict = {'length':1.0,
	      'arrow_length_ratio':0.08}

ax.quiver(0,0,0,10,0,0,**arrow_dict)
ax.quiver(0,0,0,0,10,0,**arrow_dict)
ax.quiver(0,0,0,0,0,10,**arrow_dict)
#ax.set_xlabel('x')
#ax.set_ylabel('y')
#ax.set_zlabel('z')
ax.axis('off')
ax.auto_scale_xyz([0, 11], [0, 11], [0, 11])
xvec = dt.Vector(1.0, 0.0, 0.0)*10.
yvec = dt.Vector(0.0, 1.0, 0.0)*10.
zvec = dt.Vector(0.0, 0.0, 1.0)*10.
delta = xvec.length()/10.0
ax.text(xvec.x,xvec.y,xvec.z+delta,r'$\vec{a}$')
ax.text(yvec.x,yvec.y,yvec.z+delta,r'$\vec{b}$')
ax.text(zvec.x,zvec.y,zvec.z+delta,r'$\vec{c}$')
u_alpha = dt.Vector(0.0, 1.0, 0.0)
u_beta = dt.Vector(0.0, 0.0, 1.0)
u_gamma = dt.Vector(0.0, 0.0, 1.0)
alpha_label_pos = (dt.Vector(0,1,0) + dt.Vector(0,0,1))*0.8
ax.text(alpha_label_pos.x, alpha_label_pos.y, alpha_label_pos.z, r'$\alpha$')
beta_label_pos = (dt.Vector(1,0,0) + dt.Vector(0,0,1))*1.2
ax.text(beta_label_pos.x, beta_label_pos.y, beta_label_pos.z, r'$\beta$')
gamma_label_pos = (dt.Vector(0,1,0) + dt.Vector(1,0,0))*1.6
ax.text(gamma_label_pos.x, gamma_label_pos.y, gamma_label_pos.z, r'$\gamma$')

dth = 0.001
th_total = 0.0

alpha_curve_x = []
alpha_curve_y = []
alpha_curve_z = []

alpha_curve_x.append(u_alpha.x)
alpha_curve_y.append(u_alpha.y)
alpha_curve_z.append(u_alpha.z)

beta_curve_x = []
beta_curve_y = []
beta_curve_z = []

beta_curve_x.append(u_beta.x)
beta_curve_y.append(u_beta.y)
beta_curve_z.append(u_beta.z)

gamma_curve_x = []
gamma_curve_y = []
gamma_curve_z = []

gamma_curve_x.append(u_gamma.x)
gamma_curve_y.append(u_gamma.y)
gamma_curve_z.append(u_gamma.z)

while math.degrees(th_total) < 90.0:
    u_alpha = rot_z(dth, u_alpha)
    u_beta  = rot_y(dth, u_beta)
    u_gamma = rot_x(dth, u_gamma)
    th_total += dth
    alpha_curve_x.append(u_alpha.x)
    alpha_curve_y.append(u_alpha.y)
    alpha_curve_z.append(u_alpha.z)
    beta_curve_x.append(u_beta.x)
    beta_curve_y.append(u_beta.y)
    beta_curve_z.append(u_beta.z)
    gamma_curve_x.append(u_gamma.x)
    gamma_curve_y.append(u_gamma.y)
    gamma_curve_z.append(u_gamma.z)

ax.plot(alpha_curve_x, alpha_curve_y, alpha_curve_z)
ax.plot(beta_curve_x, beta_curve_y, beta_curve_z)
ax.plot(gamma_curve_x, gamma_curve_y, gamma_curve_z)

plt.show()
