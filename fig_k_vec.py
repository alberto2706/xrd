import matplotlib.pyplot as plt
import draw_tools as dt
from draw_tools import Vector
from mpl_toolkits.mplot3d import Axes3D
import math

dt.ax.axis('off')

dt.draw_arrow(Vector(-2,0), Vector(-1,0))

charge_volume = Vector(0.0,1.0)
charge_perimeter = []

tth_total = 0.0

tth_delta = 0.01

drawing_vec = charge_volume

while tth_total < 2.0*math.pi:
    a = drawing_vec.rotate(math.degrees(tth_total))
    charge_perimeter.append(a)
    tth_total += tth_delta
    

from random import random, randint

curr = 0.0

for i in range(1,len(charge_perimeter)-1):
    a = charge_perimeter[i-1]
    b = charge_perimeter[i]
    c = charge_perimeter[i+1]
    r = random()/13.0 + float(curr)
    b = (b*r + a + c)*(1.0/3.0)
    if r < 0.1:
	charge_perimeter[i] = b
    if r > 1.9:
	curr += randint(1,9)*math.pow(-1, randint(1,8))
    


for i in range(1,len(charge_perimeter)):
    a = charge_perimeter[i-1]
    b = charge_perimeter[i]
    dt.draw_line(a,b)


plt.show()
