import matplotlib.pyplot as plt
import draw_tools as dt
from draw_tools import Vector
from mpl_toolkits.mplot3d import Axes3D
import math

square_lines = [(0,0),(0,1),(1,1),(1,0)]

fig = plt.figure(0)
#ax = fig.add_subplot(111, projection='3d')
#ax.set_aspect('equal')
#ax.axis('off')
axes = {}

axes[11] = plt.subplot2grid((5,6),(0,0),colspan=2)
local_lines = [(0,0),(1,0),(1.1,.1),(0.25,.1)]
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(0.2,0.6))
for point in local_lines:
    dt.draw_line(point, (point[0] + 0.2, point[1] + 0.6))

axes[12] = plt.subplot2grid((5,6),(0,2),colspan=2)
local_lines = [(0,0),(1.4,0),(1.4,2.2),(0,2.2)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[13] = plt.subplot2grid((5,6),(0,4),colspan=2)
local_lines = [(0,0),(1.4,0),(1.4,2.2),(0,2.2)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))


axes[21] = plt.subplot2grid((5,6),(1,0),colspan=2)
L, H = 2.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[22] = plt.subplot2grid((5,6),(1,2),colspan=2)
L, H = 2.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[23] = plt.subplot2grid((5,6),(1,4),colspan=2)
L, H = 1.0, 2.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))


axes[31] = plt.subplot2grid((5,6),(2,0),colspan=2)
L, H = 2.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[32] = plt.subplot2grid((5,6),(2,2),colspan=2)
L, H = 2.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[33] = plt.subplot2grid((5,6),(2,4),colspan=2)
L, H = 1.0, 2.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))


axes[41] = plt.subplot2grid((5,6),(3,0),colspan=3)
L, H = 1.0, 1.5


axes[42] = plt.subplot2grid((5,6),(3,3),colspan=3)


axes[51] = plt.subplot2grid((5,6),(4,0),colspan=2)
L, H = 1.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[52] = plt.subplot2grid((5,6),(4,2),colspan=2)
L, H = 1.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))

axes[53] = plt.subplot2grid((5,6),(4,4),colspan=2)
L, H = 1.0, 1.0
local_lines = [(0,0),(L,0),(L,H),(0,H)]
sx,sy = 0.4,0.4
dt.draw_list(local_lines, cycle=True,shift=(0.0,0.0))
dt.draw_list(local_lines, cycle=True,shift=(sx,sy))
for point in local_lines:
    dt.draw_line(point, (point[0] + sx, point[1] + sy))
plt.text(L/4.0,-0.4*H,r'C$\mathrm{\'u}$bica')

#ax11.set_aspect('equal')

for axis_id in axes:
    axis = axes[axis_id]
    axis.set_aspect('equal')
    axis.axis('off')

plt.show()
